@echo off
:start
echo "1 - Run all services present in solution"
echo "2 - Stop all services present in solution"
echo "3 - Run one specific service"
echo "4 - Stop one specific service"
echo "5 - Quit"
set /p command=command (1,2,3,4,5) : 

if %command%==1 goto runAll
if %command%==2 goto stopAll
if %command%==3 goto runOne
if %command%==4 goto stopOne
if %command%==5 goto quit


:runAll
start cmd /c "CD services/Bixi_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd services/Bus_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd services/Critics_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd services/Garden_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd services/User_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd services/Reservation_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"

start cmd /c "cd web_service & Docker-compose build & Docker-compose up -d & Docker-compose start"

goto start

:stopAll
start cmd /c "cd services/Bixi_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd services/Bus_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd services/Critics_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd services/Garden_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd services/User_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd services/Reservation_Microservice & Docker-compose stop & Docker-compose down"

start cmd /c "cd web_service & Docker-compose stop & Docker-compose down"

goto start

:runOne
echo "1 - Bixi_Microservice"
echo "2 - Bus_Microservice"
echo "3 - Critics_Microservice"
echo "4 - Garden_Microservice"
echo "5 - User_Microservice"
echo "6 - Reservation_Microservice"
echo "7 - web_service"
set /p secondcommand=secondcommand :

if %secondcommand%==1 goto runBixi
if %secondcommand%==2 goto runBus
if %secondcommand%==3 goto runCritics
if %secondcommand%==4 goto runGarden
if %secondcommand%==5 goto runUser
if %secondcommand%==6 goto runReservation
if %secondcommand%==7 goto runWeb

goto start

:runBixi
start cmd /c "CD services/Bixi_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runBus
start cmd /c "cd services/Bus_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runCritics
start cmd /c "cd services/Critics_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runGarden
start cmd /c "cd services/Garden_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runUser
start cmd /c "cd services/User_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runReservation
start cmd /c "cd services/Reservation_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:runWeb
start cmd /c "cd web_service & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:stopOne
echo "1 - Bixi_Microservice"
echo "2 - Bus_Microservice"
echo "3 - Critics_Microservice"
echo "4 - Garden_Microservice"
echo "5 - User_Microservice"
echo "6 - Reservation_Microservice"
echo "7 - web_service"
set /p thirdcommand=thirdcommand :

if %thirdcommand%==1 goto stopBixi
if %thirdcommand%==2 goto stopBus
if %thirdcommand%==3 goto stopCritics
if %thirdcommand%==4 goto stopGarden
if %thirdcommand%==5 goto stopUser
if %thirdcommand%==6 goto stopReservation
if %thirdcommand%==7 goto stopWeb

goto start

:stopBixi
start cmd /c "cd services/Bixi_Microservice & Docker-compose stop & Docker-compose down"
goto start

:stopBus
start cmd /c "cd services/Bus_Microservice & Docker-compose stop & Docker-compose down"
goto start

:stopCritics
start cmd /c "cd services/Critics_Microservice & Docker-compose stop & Docker-compose down"
goto start

:stopGarden
start cmd /c "cd services/Garden_Microservice & Docker-compose stop & Docker-compose down"
goto start

:stopUser
start cmd /c "cd services/User_Microservice & Docker-compose stop & Docker-compose down"
goto start

:stopReservation
start cmd /c "cd services/Reservation_Microservice & Docker-compose build & Docker-compose up -d & Docker-compose start"
goto start

:stopWeb
start cmd /c "cd web_service & Docker-compose stop & Docker-compose down"
goto start

:quit
