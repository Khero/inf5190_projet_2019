# INF5190_Projet_2019

This is a class project within the school Université du Québec à Montréal. This application contains seven services in total.

* Web_service
* Bus microservice
* Bixi microservice
* Critics microservice
* Garden microservice
* Reservation microservice
* User microservice

| Functionality             |    Service                    |  LocalHost              |    Swagger                      |
| ------------------------- | ----------------------------- | ----------------------- | ------------------------------- |
| Front-end app             | Web_service                   | http://localhost:3000/  |          N/A                    |
| Get Near Station Bus      | Bus_Microservice              | http://localhost:4050/  |  http://localhost:4050/swagger  |
| Get all Station Bus       | Bus_Microservice              | http://localhost:4050/  |  http://localhost:4050/swagger  |
| Get Near Station Bixi     | Bixi_Microservice             | http://localhost:4000/  |  http://localhost:4000/swagger  |
| Get all Station Bixi      | Bixi_Microservice             | http://localhost:4000/  |  http://localhost:4000/swagger  |
| Get Near Station Garden   | Garden_Microservice           | http://localhost:4100/  |  http://localhost:4100/swagger  |
| Get all Station Garden    | Garden_Microservice           | http://localhost:4100/  |  http://localhost:4100/swagger  |
| Add and delete comment    | Critics_Microservice          | http://localhost:4100/  |  http://localhost:4100/swagger  |
| Authentificate user       | User_Microservice             | http://localhost:5000/  |  http://localhost:5000/swagger  |
| Register                  | User_Microservice             | http://localhost:5000/  |  http://localhost:5000/swagger  |
| Facebook Oauth2           | User_Microservice             | http://localhost:5000/  |  http://localhost:5000/swagger  |
| Make a reservation        | Reservation_Microservice      | http://localhost:4250/  |  http://localhost:4250/swagger  |
| Sign bail                 | Reservation_Microservice      | http://localhost:4250/  |  http://localhost:4250/swagger  |





## Description

This application allows you to view garden locations on a google maps windows along with Bixi and Bus stops within a certain radius. Each user requires an account to access the application. This application allows you to make a reservation or a lease on the gardens, as well as add to favorites.

## Authors
* Andy Ton-That - TONA08119506
* Nemmas Mohamed Kheireddine - NEMM0512910508
* Luc Yeuk Lung Ma - MAXL11069304

## Docker

Use the utils.bat to run or stop the services. To use this, you must have Docker installed. 
* https://docs.docker.com/install/

Note: this script work only on windows
## Installation

Look for the README.md for each dependencies concerning each service. Otherwise run the utils.bat script and run Docker.
