# Web Service

This service is the user interface for the client. It uses React.js. This service will call the other microservices to populate, modify, delete and add gardens, bixis, reservations, users, buses, critics.

## Docker

To start this service using Docker manually, use the following commands : 

* docker-compose build
* docker-compose up
* docker-compose start

Here is a list of useful Docker commands to run or stop the service :

* docker-compose stop (stops the service)
* docker-compose down (unloads the container)

The Docker container is opening up to port **3000**

## Dependencies

react Datepicker
* http://react-day-picker.js.org/
* npm install react-day-picker --save

react ratings
* https://github.com/ekeric13/react-ratings-declarative
* npm install --save react-ratings-declarative

react
* npm install -g create-react-app@3.0.1 --silent

Node Js Express
* npm install --silent

react-moment
* npm install --save moment react-moment

Swagger
* npm install swagger-jsdoc
* npm install swagger-ui-express