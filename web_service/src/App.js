import React, {useEffect } from 'react';
import { BrowserRouter as Router, Route , Switch} from 'react-router-dom';
import Register from './Components/Auth/Register';
import Login from './Components/Auth/Login';
//import Landing from './Components/Layout/Landing';
import Alerts from './Components/Layout/Alerts';
import NaviBar from './Components/Layout/Navbar';
import Dashboard from './Components/MainPage/Dashboard';
import Profile from './Components/Profile/Profile';
import APropos from './Components/APropos/APropos.js';
import Contact from './Components/Contact/Contact.js';
import Favorite from './Components/Favorite/Favorite';
import setAuthToken from './utils/setAuthToken';
import { loadUser } from './Actions/auth';
import PrivateRoute from './Components/Routing/PrivateRoute';
import MapGoogle from './Components/Map/MapContainer'
import NotFound from './NotFound/NotFound'
//Redux
import { Provider } from 'react-redux';
import store from './store';


if(localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
    useEffect(() => {
      store.dispatch(loadUser());
      
    }, []);

    return(
        <Provider store={store}>
          <Router>
                  <NaviBar/>
                  <Alerts/>
                    <Switch>
                      <PrivateRoute exact path="/" component={Dashboard} />
                      <Route exact path="/register" component={Register} />
                      <Route exact path="/login" component={Login} />
                      <Route exact path="/propos" component={APropos} />
                      <Route exact path="/contact" component={Contact} />
                      <PrivateRoute exact path='/home' component={Dashboard} />
                      <PrivateRoute exact path='/profile' component={Profile} />
                      <PrivateRoute exact path='/favorite' component={Favorite} />
                      <PrivateRoute exact path='/map' component={MapGoogle} />
                      <Route path="*" component={NotFound} />
                    </Switch>
            </Router>
        </Provider>  
    );
};

export default App;