// components/NotFound.js
import React from 'react';

const NotFound = () =>
    <div>
        <h3>Erreur 404 page non trouvé</h3>
        <p>Nous sommes désolés mais la page que vous recherchez n'existe pas.</p>
    </div>
export default NotFound;