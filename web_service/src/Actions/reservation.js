
import axios from 'axios';
import { SIGNED_FAIL, SIGNED_SUCCESS, RESERVATION_SUCCESS, RESERVATION_FAIL, GET_RESERVATION,
    GET_BAIL, REMOVE_RESERVATION } from './types';
import { setAlert } from './alert';


export const getMyBail= (user_id) => async dispatch => {
    try {
        
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        
        const user = {
            user_id: user_id
           
         }        
            const body = JSON.stringify(user);
            const res = await axios.post(`http://localhost:4250/api/reservation/bail`, body, config);
    
            dispatch({
                type:GET_BAIL,
                payload:res.data
            });
            

    } catch (err) {
        
        dispatch({
            type: SIGNED_FAIL,
            payload: {msg: 'Internal Server Error',status: 500}
        });
    }

}




export const getReservation = (user_id) => async dispatch => {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const user = {
            user_id: user_id
           
    }        
    const body = JSON.stringify(user);
    
    try {
        const res = await axios.post(`http://localhost:4250/api/reservation/reserve`, body, config);

        dispatch({
            type:GET_RESERVATION,
            payload:res.data
        });


    } catch (err) {
        
        dispatch({
            type: RESERVATION_FAIL,
            payload: {msg: 'Internal Server Error',status: 500}
        });
    }

}



export const signBail = (infoData) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    
    try {
        const res = await axios.post(`http://localhost:4250/api/reservation/newBail`, infoData, config);

        dispatch({
            type:SIGNED_SUCCESS,
            payload:res.data
        });
        dispatch(setAlert('Bail ajouté', 'success'));
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }
        dispatch({
            type: SIGNED_FAIL,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
        
    }
}


export const createReservation = (infoData,infoDate) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
     const user = {
            user_id: infoData.user_id,
            user_name: infoData.user_name,
            place_id: infoData.place_id,
            place_name: infoData.place_name,
            date: infoDate.date,
            }        
        const body = JSON.stringify(user);
    
    try {
        const res = await axios.post(`http://localhost:4250/api/reservation/create`, body, config);

        dispatch({
            type:RESERVATION_SUCCESS,
            payload:res.data
        });
        dispatch(setAlert('Reservation ajoutée', 'success'));

    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }
        dispatch({
            type: RESERVATION_FAIL,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
        
    }
}

export const deleteReservation = (infoData,infoDate) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
     const user = {
            user_id: infoData.user_id,
            place_id: infoData.place_id,
          
            }        
        const body = JSON.stringify(user);
    
    try {
        const res = await axios.post(`http://localhost:4250/api/reservation/del`, body, config);

        dispatch({
            type:REMOVE_RESERVATION,
            payload:res.data
        });
        dispatch(setAlert('Reservation supprimée', 'warning'));

    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }
        dispatch({
            type: RESERVATION_FAIL,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
        
    }
}



