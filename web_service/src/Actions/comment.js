import axios from 'axios';
import { ADD_COMMENTS, COMMENTS_ERROR, REMOVE_COMMENTS, GET_COMMENTS,GET_COMMENT} from './types';
import { setAlert } from './alert';

// Get All comments

export const getAllComments= () => async dispatch => {
    
    try {
        const res = await axios.get(`http://localhost:4150/api/critics/all`);

        dispatch({
            type:GET_COMMENTS,
            payload:res.data
        });
    } catch (err) {
        dispatch({
            type: COMMENTS_ERROR,
            payload: {msg: 'Internal Server Error',status: 500}
        });
        
    }
}



export const getCommentForPlace = id => async dispatch => {

    try {
        const res = await axios.get(`http://localhost:4150/api/critics/place/${id}`);

        dispatch({
            type:GET_COMMENT,
            payload:res.data
        });
    } catch (err) {

        dispatch({
            type: COMMENTS_ERROR,
            payload: {msg: 'Internal Server Error',status: 500}
        });
        
    }
}






export const addComment= (infoData,formData) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const user = {
            user_id: infoData.user_id,
            user_name: infoData.user_name,
            place_id: infoData.place_id,
            place_name: infoData.place_name,
            text: formData.text,
    }        
    const body = JSON.stringify(user);
    
    try {
        const res = await axios.post(`http://localhost:4150/api/critics/addComment`, body, config);

        dispatch({
            type:ADD_COMMENTS,
            payload:res.data
        });
        dispatch(setAlert('Commentaire ajouté', 'success'));
    } catch (err) {
        dispatch({
            type: COMMENTS_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
        
    }
}

export const deleteComment= (post_id,comment_id) => async dispatch => {
 
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const id = {
            place_id: post_id,
            comment_id: comment_id
          
    }        
    const body = JSON.stringify(id);
    try {
        const res = await axios.post(`http://localhost:4150/api/critics/delete`,body,config);

        dispatch({
            type:REMOVE_COMMENTS,
            payload:res.data
        });
        dispatch(setAlert('Commentaire supprimé', 'warning'));
    } catch (err) {
        dispatch({
            type: COMMENTS_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
        
    }
}