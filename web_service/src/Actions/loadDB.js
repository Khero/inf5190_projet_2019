import axios from 'axios';
import { GARDENS_LOADED, GARDENS_LOAD_ERROR, BIXI_LOADED, BUS_LOADED, BUS_LOAD_ERROR, BIXI_LOAD_ERROR } from './types';

//get all the gardens from database
export const loadGardens = () => async dispatch => {
    try {
        const res = await axios.get('http://localhost:4100/api/garden');
        dispatch({
            type: GARDENS_LOADED,
            payload: res.data
        });
        
    } catch (err) {
        dispatch({
            type: GARDENS_LOAD_ERROR,
            payload: {msg: 'Internal Server Error',status: 500}
        });
    }
}

export const loadBixi = () => async dispatch => {
    try {
        const res = await axios.get('http://localhost:4000/api/bixi');
        dispatch({
            type: BIXI_LOADED,
            payload: res.data
        });
        
    } catch (err) {
        dispatch({
            type: BIXI_LOAD_ERROR,
            payload: {msg: 'Internal Server Error',status: 500}
        });
    }
}

export const loadBus = () => async dispatch => {
    try {
        const res = await axios.get('http://localhost:4050/api/bus');
        dispatch({
            type: BUS_LOADED,
            payload: res.data
        });
        
    } catch (err) {
        dispatch({
            type: BUS_LOAD_ERROR,
            payload: {msg: 'Internal Server Error',status: 500}
        });
    }
}
