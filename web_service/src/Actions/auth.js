import axios from 'axios';
import { REGISTER_SUCCESS, REGISTER_FAIL, AUTH_FAIL, USER_LOADED , LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, CLEAR_PROFILE, CLEAR_RESERVATION} from './types';
import { setAlert } from './alert';
import { getCurrentProfile } from './profile';
import setAuthToken from '../utils/setAuthToken';

//Register user 

export const register = ({ name, email, password}) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const user = {
        method: ['local'],
        local:{
            name,
            email,
            password
        }        
    }
    const body = JSON.stringify(user);

    try {
        const res = await axios.post('http://localhost:5000/api/users', body, config)
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data
        });
        
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }
        
        dispatch({
            type: REGISTER_FAIL
        });
        
    }

}

//Load user
export const loadUser = () => async dispatch => {
    if(localStorage.token) {
        
        setAuthToken(localStorage.token);
    }
    
    try {
        const res = await axios.get('http://localhost:5000/api/auth');
        dispatch({
            type: USER_LOADED,
            payload: res.data
        });
        dispatch(getCurrentProfile());

    } catch (err) {
        dispatch({
            type: AUTH_FAIL
        });
    }
}

//Login user actions

export const login = (email, password) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
  
    const body = JSON.stringify({email,password});

    try {
        const res = await axios.post('http://localhost:5000/api/auth', body, config)
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        });
        dispatch(loadUser());
        
    } catch (err) {
         dispatch(setAlert('Email or Password incorrect', 'danger'));
        
        dispatch({
            type: LOGIN_FAIL
        });
        
    }

};


export const oauthFacebook = (accessToken) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    
    const body = JSON.stringify({access_token: accessToken});

    try {
        const res = await axios.post('http://localhost:5000/api/auth/oauth/facebook', body, config)
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        });
        dispatch(loadUser());
        
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }
        
        dispatch({
            type: LOGIN_FAIL
        });
        
    }

};

// Logout /Clear profile

export const logout = () => dispatch =>{
    dispatch({ type: CLEAR_RESERVATION});
    dispatch({ type: CLEAR_PROFILE });
    dispatch({ type: LOGOUT });
}