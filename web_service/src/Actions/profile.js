import axios from 'axios';
import { GET_PROFILE, PROFILE_ERROR ,ACCOUNT_DELETED, ACCOUNT_DELETE_ERROR ,
ADD_FAVORITE, REMOVE_FAVORITE,FAVORITE_ERROR } from './types';
import { setAlert } from './alert';
import { logout } from './auth';


//get the current profile

export const getCurrentProfile = () => async dispatch => {

    try {
        const res = await axios.get('http://localhost:5000/api/profile/authUser');

        dispatch({
            type: GET_PROFILE,
            payload: res.data
        });
    } catch (err) {
        dispatch({
            type: PROFILE_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
    }
}

//Update profile
export const updateProfile = (formData) => async dispatch => {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    try {
        const res = await axios.post('http://localhost:5000/api/profile',formData, config);

        dispatch({
            type: GET_PROFILE,
            payload: res.data
        });

        dispatch(setAlert('Profile Updated', 'success'));
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type: PROFILE_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
    }
}

//delete account and profile data

export const deleteAccountProfile = () => async dispatch => {


    try {
        const res = await axios.delete('http://localhost:5000/api/profile');

        dispatch({
            type: ACCOUNT_DELETED,
            payload: res.data
        });
        dispatch(logout());

        dispatch(setAlert('Account Deleted Successfuly', 'warning'));
    } catch (err) {
      
        dispatch({
            type: ACCOUNT_DELETE_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
    }
}



//Add favorite
export const addFavorite = (name, db_id) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    const formData =JSON.stringify({name: name, DB_ID: db_id})
    try {
        const res = await axios.put('http://localhost:5000/api/profile/favorite',formData, config);

        dispatch({
            type: ADD_FAVORITE,
            payload: res.data
        });
        dispatch(getCurrentProfile());
        dispatch(setAlert('Added to favorite', 'success'));
    } catch (err) {
        const errors = err.response.data.errors;
        if(errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type: FAVORITE_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
    }
}

//delete favorite
export const deleteFavorite = (id) => async dispatch => {
    try {
        const res = await axios.delete(`http://localhost:5000/api/profile/favorite/${id}`);

        dispatch({
            type: REMOVE_FAVORITE,
            payload: res.data
        });
        dispatch(getCurrentProfile());
        dispatch(setAlert('Favorite deleted', 'warning'));
    } catch (err) {
     
        dispatch({
            type: FAVORITE_ERROR,
            payload: {msg: err.response.statusText, status: err.response.status}
        });
    }
}


