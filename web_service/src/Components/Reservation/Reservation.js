import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Button, Form, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import {
  createReservation,
  deleteReservation,
} from "../../Actions/reservation";
import ReservationItem from './ReservationItem';

//import ReservationItem from './ReservationItem'

const Reservation = props => {
  const [infoDate, setInfoDate] = useState({
    date: ""
  });

  const [infoData, setInfoData] = useState({
    user_id: "",
    user_name: "",
    place_id: "",
    place_name: ""
  });

  const onChange = e => {
    setInfoDate({ ...infoDate, [e.target.name]: e.target.value });
  };

  const onSubmitReservation = e => {
    e.preventDefault();
    //console.log(infoData,formData);
    props.createReservation(infoData, infoDate);
  };


  useEffect(() => {
    const initInfo = () => {
      props.auth && props.auth.method[0] === "local"
        ? setInfoData({
            place_id: props.selectedGarden.garden._id,
            place_name: props.selectedGarden.garden.Text_,
            user_id: props.auth._id,
            user_name: props.auth.local.name
          })
        : setInfoData({
            place_id: props.selectedGarden.garden._id,
            place_name: props.selectedGarden.garden.Text_,
            user_id: props.auth._id,
            user_name: props.auth.facebook.name
          });
    };
    initInfo();
  }, [props]);

  return (
    <Fragment>
      <div>
        <Form onSubmit={e => onSubmitReservation(e)}>
          <Row>
            <Col>
              <Form.Group controlId="formGridZip">
              <Form.Label>Date</Form.Label>
                <Form.Control
                  type="Date"
                  name="date"
                  value={infoDate.date}
                  onChange={e => onChange(e)}
                  required
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button variant="primary" type="submit">
                Réserver
              </Button>
            </Col>            
          </Row>
          <Row>
          <Col>
             <ReservationItem infoData={infoData}/>              
            </Col>
          </Row>
        </Form>
      </div>
    </Fragment>
  );
};

Reservation.propTypes = {
  auth: PropTypes.object.isRequired,
  createReservation: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth.user
});
export default connect(
  mapStateToProps,
  { createReservation, deleteReservation }
)(Reservation);
/*
<Button variant="danger" onClick={e => onDeleteReservation(e)}>
                delete
              </Button>*/