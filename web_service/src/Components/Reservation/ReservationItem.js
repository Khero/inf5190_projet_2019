import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Row, Col, DropdownButton, Dropdown } from "react-bootstrap";
import { deleteReservation, getReservation } from "../../Actions/reservation";
import Moment from 'react-moment';

const ReservationItem = ({
  infoData,
  getReservation,
  deleteReservation,
  reservation
}) => {
  useEffect(() => {
    getReservation(infoData.user_id);
  }, [getReservation, infoData.user_id]);

  const onDeleteReservation = e => {
    e.preventDefault();
    deleteReservation(infoData);
  };

  const smallerPicture = {
    width: "20px",
    height: "auto"
  };

  return (
    <Fragment>
      <div>
        {reservation.Reservations &&
          reservation.Reservations.map(reserv => (
            <div key={reserv._id}>
              {reserv.place_id === infoData.place_id &&
                reserv.user_id === infoData.user_id && (
                  <div>
                    <br />
                    <Row>
                      <Col>
                        <img
                          style={smallerPicture}
                          src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABEVBMVEX///8A3cIAzLPGFGnkGHr/eVYAZloAYFPFCmfkD3gA274A1bsAya6DqaPCAFz/c03/fVvDAF//fFTCAGn/bkX4/v3MCmn/qJXY9fHw/PqH69x03c3/3dXjAHP/rZv/4tyo6d9B4stx6Nb02uW78+rl+fZA1L+U5NjA7+htV2nG9e6e7+LMOHr34uvIHm/7dWXnXmrzX3LVKHnimbdq28pW5NBY2MXnrsX89figPW26KWwJamA6ZGRgYGyd5tv/im3nN4j2wNbtfazae6L0s83/uaLtRHPZQmfmLn3rQHrUPm39emHSVIragKPswdHkorzORoHWaZetbowkZF6Oq6cks6AWhHcYvqkYloYQb2Man46B8sLjAAALMElEQVR4nN2de3vbxBLGI9khjivcOOASJw1Yqe3k5FagTVrulB4Ot9CUWyl8/w9yZPkm7XVm95Wtzfs8/Qebyr++szszq9VqY2O1evjNwxVfccX64P3N9z9Y94+oUA83729uZn/urI2ZgVPdURunBk51J21cGHhHbXy4c3+zrPs7d8pGwcA7Z+NHkoFzGz9a90+j6tsL06cfqgyc2fih6X+8OMf+TA8dHr7QfqYz0G7jq26rih/rpO3tw+80HxkMtNj4stOqEWGzeaiMVLOBJhsvzruNRq0Im9uH/5X+u9VArY0vuq1G3QgzG4VIpRiosfFlp9GoIaEQqUQDFTbmEVpLwmKk0g2UbHyWR2g9CZeRyjKwbOMsQutK2DxsXvANLNh4cdlt1JtwEqnf8w2c2fj9MkLrS5jZ+L+dXTfCnR8KBtaYsNn88bEL4u7jxnuNQAibzU/4iLsfC3z1JmyyI3XnBwmw3oTMSN19fCkD1pyQFamKCA2BsPkTNVJVERoEITFS1REaBiEpUjURGgrhtjVSd37WAgZBaItUfYSGQ2iMVEOEhkSon1NNERoUoSZSzREaFqEyUi0RGhqhHKm2CA2OsNksRao9QgMkLEYqIUJDJFxE6i4lQoMknEVqFqEtO12ghJNIJUZoqITNn34hRmiohNvN94gRGirhp3S8MAkvWYDhEX7KBAyOkBehARJyDQyMcJsdoaER8iM0MEIXAwMidIvQkAjdIjQgQlcDQyF0jtBQCN0jNBBCHwNDIPSK0BAI/SI0AEJfA+tO6B2hdSf0j9CaEyIMrDMhxsAaE8IA60oIitDaEkLm0DoT4iK0noQ/Ig1cNWHP9OF2FQbaCI2/iK9haieEA5oJxydQwqg9sBGCI9RGOEhiJOB+O2qPjIR4A82EoyROjnGAo3YURYY43a4G0EQ4juM4MfyjM5VmgFF7X/t5FRFqJjxIMsJ4jAIcTizMEE91X2Dc9MQQ7uWAcTLEAPaiuXQTdKsaxFZD94viuTAp42hBeKX5BnVvBZfwUnO962QGmBwgAPfaC8K2JirOKyLUPOk8TBYeJnsAwjQqSD173VREeKO82mgJCJlsztpFQnXKeFkR4a/Kq40LgHEy8AXslwA1KeNJ1/5zHdR9orrYcdHCDLHvSXgUldVWBf6righfKa61VwaM46d+gHvtSJTiW4869p/roI7qAfhYlOdkk0qA0ZHiaxWNQ8WVDkQLPSeboWyhMmVUMpmqptITGdCvspH5JpLHdiVTTfdWuk5f5pvIHXCgsFCZMioZiJ1H0nXGSkCPjKG2MGqfSd+sIEoV5358rYhRLxM1FqpSRgU5X873UqLwNlHDN5H41QrCVA5SDZ+7iaqJdG6iVNrAi2+57FYkioWJbqtSqcFDqRu+Rc+m3WfCFVSJYiGnnKgoZ4oSe080oTjP9Ax8joWNWJEKErthcEqUkuG1kTB2aIV7ZgullPEcupQhLWAMTDE6MZG/nqFNFQtEoRuGjkRxFI4sgC4JI7UAyqUNcLVGmkjVxUxR7LlmZLNQThnAnCjmwmObhQ7Lw/tWQDll/IqKU7G5P7UD8tf4CYCRlDJAab/1Wvgxdr6JeICWZDiXkDIw82mr9bz81z4lWMgO0zOah2I3/AoxFMVBOCQBxsnXLMKURhi1hW74mT9iR1h/6tMAmbNpnxakkZwynvgidsR61J4o5iZykr6hrRBNFLsMT0QJkJAo5oScBsNSk5YQxZr31gdRAtR2vQpxalM6YCR3wy86rjNqSxyD1EQxEx2QUNAUJC2gXjTcELvn0hIwLVHMw5S+wE8fhnmcSuuVz28cIrXV+U36IcauVyakD0RKyVaU/G9XPh2QBNiSzyLWLI9qCemFW8okVNwbzmzkMGYGPpf/EkvXK+maTMgEVC2gZvXNOZmx1Xktr/4alke1ogLyJpocUblMQmTM+FS30exdryRyaXrKJlTec5sw3nQsA7LV7fym8m+DUcwsCalTDbHsLpmouuc20cWTSz1khnd+qxh/uQzLo1pCavHNqGiWiNrtRBuPbl93ug9EytaDTufmVn9cPaXrlUStalIHQv12oinl7599fvlgqcvPP/tdE5xTmZdHdaK2F06A2u1EU7279c4Xf3z51X8m+urLP754Z+td4/e5iWImGmDfjVCZMoqEZZkJbcujOtEaKH6ymCGa5moeIT9RTEWsTF2SRS7TTmkeIT9RzAhpty94dXfRRP0OVB4hvesVCWkJ0ZnQlDI4hJyu14nQIeEvpB3pDEK3RDElHJAIub1TUdqUwSBkdb0CIa1/cilp5tLtQGUQEpdH1YSGmQBEqNuBSidkdr2CaGWbH6EmZZAJXRPF6gg1pQ2V0DlRrI5Q0w0TCd0TxQoJ1d0wkdCPj7qd1ptQ1Q3TCB263rV4qEoZJELe8ugaCVULqBRCv0TBIPSpaWaSUwaF0C9RTESsaXzq0pnklEEg5C+PyoS0pSjrXiEKopgy7ISuXW+JcEAidO+eimIT+vORuyfnHr8osRu2EnonipxQ36AW5bpOIyCWL2Yj9E8UOSFtWd9xrU1SqRu2EHp0vSUR75GCCEvdsIXQcXlUEg1wI8UQlh7hNxO6Lo+Koq55A4qaKWJhVBgJEYkiF/U5NkDKn6pQ2hgJ/YuZqci3uTEJMSqlDBOhZ9dbIKTePyRuTKQgLlKGgdC36y0QknfswwiXKcNAiOLLCKmAGymOcJ4y9IQey6Oi6LsTUZNptOyGtYQ+y6OCGOcPwKaaaLEDVUdI3j1KIaTvicJUpjOlRkJUosgJGfuggYCzlKEhhCWKXHRA5ECcdcNqQlyimIizvxTR5hekJUTy8fYIQwdivoCqJIR0vUtC1vkRKZQwSxkqQkzXuxDvySfAimJJPQUhquudiflYEK40nerqT4nwT1TXOydkPkaKBYyivyTCv7CA7Oe50WEavSkjbr25h+VjP7sGnk0zCR5i+VyOp0vRhH8XTcTHKP959TO4iW+XiFtvwTHq8hww/eEusv5ZEP4D5nN6lhtbm+aK5yZu/Q0ndDmaDp0SM/07Rdz6Fx2jjodFpXDC6E1OiE4UsetZUchOf6Z7ExO34HyuZ5vA65ooL23wiSJ2PmQI3CXmegsvZmKf077wgFnKgPPFHod9VWEi3kGv0yHxgFUQ+hx7XcV0CudznUinSgMg9DukFbIvo1pC34N2r+pO6H1YMrwThhP6nkEL7xPRixcDX0D4ZAMmRBw8D+6isISYlwfsQxGhhMxTd7RKa0uIejkCdD5FEvrPo3Mh51MgIerlDxMBV6VwhJgXI8xVR0LYG0pyjWpICBuEU8FKcBQhccMzQ6jZBkSIqNZEHWEQMYTEh0WZwjRSEMLE85UPOqV1IUzoxyWtARFBiM0TJdWDsELAwsut1kg4Br/5EI3oS5hU6WCOmK6XsHLATKlfXvQjrCpNlOWX+r0Ise2EXl7LGj6EVZRqavnczvAhhBfbeu2tgTBBt0tm9a5cbXQlXM0cU5TrYHQkXN0QXMoxUp0IkzHutbEM9ZzShgsh8s2/PJ2uhDCJEa/idJSDjWzC9Rk41R63iGMSJtdrGYElDSMWI4swiYHL2h5iLcMxCJN1B+hSPUZyJBNmfJW2ukz1yVMOkTDjW2mRRlB/nzYeSYRJDLr1iVVvQJlX7YRJMvba5VSp9o6sRtoIk/h4/fnBpN7wqm2ENBImyXV97VuqPzBBagmTJH56UqfZ06jeMJt31JRKwiQbewchuFdSf7iftmVMiXBCt39St9RAVW/vbD+NoiLovQJZFpjjg8EaOweUeqPT4dn+0VWazgjH4+uDg+PByWgVw+7/mVGOqALQRsQAAAAASUVORK5CYII="
                        alt="desc"
                        />
                        <b>{reserv.place_name}</b>
                      </Col>
                      <Col sm={{offset: 2}}>

                      <DropdownButton id="dropdown-basic-button" variant="secondary" title="" size="sm">
                        <Dropdown.Item href="#/action-1" variant="danger"
                      onClick={e => onDeleteReservation(e)}>Supprimer</Dropdown.Item>
                      </DropdownButton>
                      </Col>
                    </Row>

                    <Row className="dateField">
                      <Col>
                        <p><Moment format='DD/MM/YYYY'>{reserv.date}</Moment></p>
                      </Col>
                    </Row>
                  </div>
                )}
            </div>
          ))}
      </div>
    </Fragment>
  );
};

ReservationItem.propTypes = {
  infoData: PropTypes.object.isRequired,
  deleteReservation: PropTypes.func.isRequired,
  getReservation: PropTypes.func.isRequired,
  reservation: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth.user,
  reservation: state.reservation
});
export default connect(
  mapStateToProps,
  { deleteReservation, getReservation }
)(ReservationItem);
