// components/NotFound.js
import React from 'react';
import { Col, Row} from 'react-bootstrap';

import "./Contact.css"




const Contact = () =>




        <Row className="justify-content-md-center">
            <Col xs lg="2">
            </Col>
            <Col md="auto"><div className="div-reg4">
                    <h3>Contactez-nous par courriel</h3>
                    <a href="mailto:servicesinformatiques.uqam.ca">serviceinformatique@equi.ca</a>

                    <h3>Contactez-nous par téléphone</h3>
                    <a href="callto:15149873000">514 987-3000</a>

                    <h3>Adresse postale</h3>
                    Université du Québec à Montréal<br />
                    Case postale 8888, succursale Centre-ville<br />
                    Montréal (Québec) H3C 3P8 Canada
                </div>

           </Col>
            <Col xs lg="2">
            </Col>
        </Row>

export default Contact;