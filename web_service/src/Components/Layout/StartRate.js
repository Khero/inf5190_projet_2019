import StarRatingComponent from 'react-star-rating-component';
import React, {Component} from 'react'
import axios from 'axios';


 
class Rate extends Component {
    constructor() {
        super();
    
        this.state = {
          rating: 0,
          average:0,
        };
      }

      getRating= async (user_id,place_id) =>{
        
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const data = {
                    user_id: user_id,
                    place_id: place_id
                }
            const body =JSON.stringify(data);
            const res = await axios.get('http://localhost:4150/api/critics/getRating',body,config).then(res=>
                   { 
                       this.setState({...this.state,rating: res.data.rating});
                    }
            ).catch((err) => {
                console.log(err);
        });
    }   
        
    updateRating= async (user_id,place_id,rating) =>{
        
            const config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const data = {
                    user_id: user_id,
                    place_id: place_id,
                    rating: rating
                }
            const body =JSON.stringify(data);
            const res = await axios.post('http://localhost:4150/api/critics/rate',body,config).then(res=>
                   { 
                       this.setState({...this.state,rating: res.data.rating});
                       this.getAvgRate(place_id);
                    }
            ).catch((err) => {
                console.log(err);
        });   
    }


    getAvgRate =  async (place_id) =>{
        
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const data = {
                place_id: place_id
            }
        const body =JSON.stringify(data);
        const res = await axios.post('http://localhost:4150/api/critics/rate',body,config).then(res=>
               { 
                   this.setState({...this.state,average: res.data.avgRating});
        
                }
        ).catch((err) => {
            console.log(err);
    });   
}

    componentDidMount(){
        this.getRating(this.props.user_id,this.props.place_id).then(()=> {
            this.getAvgRate(this.props.place_id);
          })
    }
    
      onStarClick(nextValue, prevValue, name) {
        this.setState({...this.state,rating: nextValue});
        this.updateRating(this.props.user_id,this.props.place_id,nextValue);
        this.getAvgRate(this.props.place_id);

       
      }
    
      render() {
        const { rating} = this.state;
        
        return (                
          <div>
            <h2>Moyenne: {rating}</h2>
            <StarRatingComponent 
              name="rate1" 
              starCount={5}
              value={rating}
              onStarClick={this.onStarClick.bind(this)}
            />
          </div>
        );
      }
}

 
 export default Rate;