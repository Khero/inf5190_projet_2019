import React,{Fragment} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import { logout } from '../../Actions/auth';
import { connect } from 'react-redux';
import { LinkContainer } from "react-router-bootstrap";
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle} from 'reactstrap';
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Navibar extends React.Component {

    universalOptions =(
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Options
            </DropdownToggle>
            <DropdownMenu right>
                <LinkContainer to="/propos">
                    <DropdownItem>
                        À propos
                    </DropdownItem>
                </LinkContainer>
                <LinkContainer to="/contact">
                    <DropdownItem>
                        Contact
                    </DropdownItem>
                </LinkContainer>
            </DropdownMenu>
        </UncontrolledDropdown>
    );

    authNav =(
        <Nav>
            <LinkContainer to="/map">
                <Nav.Link>Map</Nav.Link>
            </LinkContainer>

            {this.universalOptions}

            <UncontrolledDropdown nav direction="down" >
                <DropdownToggle>
                    <FontAwesomeIcon icon={faUser}/>
                </DropdownToggle>
                <DropdownMenu right>
                    <LinkContainer to="profile">
                        <DropdownItem>Profile</DropdownItem>
                    </LinkContainer>
                    <DropdownItem divider />
                    <DropdownItem onClick={this.props.logout}>Logout</DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </Nav>
    );

    guestNav =(
        <Nav>

            {this.universalOptions}

            <LinkContainer to="/register">
                <Nav.Link>Enregistrement</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/login">
                <Nav.Link>Se Connecter</Nav.Link>
            </LinkContainer>
        </Nav>
    );

    render() {
        return(
        <Fragment>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <LinkContainer to="/home">
                    <Navbar.Brand>Gestion de Jardins</Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav  className="mr-auto">
                    </Nav>

                    {!this.props.auth.loading && (<Fragment>
                            {this.props.auth.isAuthenticated ? this.authNav : this.guestNav }
                        </Fragment>
                    )}
                </Navbar.Collapse>
            </Navbar>

        </Fragment>
        );
    }
}

const mapStateTopProps = state => ({
    auth: state.auth
});
export default connect(mapStateTopProps,{ logout })(Navibar);