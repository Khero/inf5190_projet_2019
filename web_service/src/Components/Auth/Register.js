import React,  { Fragment , useState } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import { setAlert } from '../../Actions/alert';
import { register , oauthFacebook } from '../../Actions/auth';
import { Redirect } from 'react-router-dom';
import './Register.css'
import FacebookLogin from 'react-facebook-login';

const Register = (props) =>{

    const [formData, setFormData] = useState ({
        name:'',
        email:'',
        password:'',
        password2:''
    });

    const { name, email, password, password2 } = formData;

    const onChange = e => 
        setFormData({...formData,[e.target.name]: e.target.value});
    
    const onSubmit = async e => {
            e.preventDefault();
            if(password !== password2 ){
                await props.setAlert('Passwords do not match','danger');

            }else{
                await props.register ({ name, email, password });      
            }
    }
    const responseFacebook = async (res) => {
        /*console.log(res.accessToken);*/
        await props.oauthFacebook(res.accessToken);
      }
    
    if(props.isAuthenticated){
        return <Redirect to="/home" />;
    }
    
        return(
         <Fragment>
             <div className={"divStyle"}>
             <Row className="justify-content-md-center">
                 <Col xs lg="2">
                 </Col>
                 <Col md="auto"><div className="div-reg">
                     <h4 className="card-title mt-3 text-center">Create new account</h4>
                     <Form onSubmit={e=> onSubmit(e)}>
                         <Form.Group  controlId="formGridName">
                             <Form.Label>Name</Form.Label>
                             <Form.Control type="name" placeholder="Enter name"
                                           name="name"
                                           value={name}
                                           onChange ={e => onChange(e)}/>
                         </Form.Group>
                         <Form.Group  controlId="formGridEmail">
                             <Form.Label>Email</Form.Label>
                             <Form.Control type="email" placeholder="Enter email"
                                           name="email"
                                           value={email}
                                           onChange ={e => onChange(e)}/>
                         </Form.Group>

                         <Form.Group  controlId="formGridPassword">
                             <Form.Label>Password</Form.Label>
                             <Form.Control type="password" placeholder="Password"
                                           name="password"
                                           value={password}
                                           onChange ={e => onChange(e)} />
                         </Form.Group>

                         <Form.Group  controlId="formGridPassword2">
                             <Form.Label> Password confirmation</Form.Label>
                             <Form.Control type="password" placeholder="Repeat password"
                                           name="password2"
                                           value={password2}
                                           onChange ={e => onChange(e)}/>
                         </Form.Group>

                         <Button variant="success"  type="submit" block>
                             Register
                         </Button>
                         <h5 className="text-center">Or</h5>
                         <FacebookLogin
                             appId="2260756780842939"
                             textButton="Register with Facebook"
                             fields="name,email"
                             cssClass="loginBtn loginBtn--facebook"
                             callback={responseFacebook} />
                     </Form>
                 </div></Col>
                 <Col xs lg="2">
                 </Col>
             </Row>
             </div>
         </Fragment> 
            
        );
    }
const mapStateTopProps = state => ({
        isAuthenticated: state.auth.isAuthenticated
});
export default connect(mapStateTopProps ,{ setAlert , register, oauthFacebook})(Register);