import React,  { Fragment , useState } from 'react';
import { Button, Form, Col, Row } from 'react-bootstrap';
import './Register.css'
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { setAlert } from '../../Actions/alert';
import { login , oauthFacebook } from '../../Actions/auth';
import FacebookLogin from 'react-facebook-login';

const Login = (props) =>{


    const [formData, setFormData] = useState ({
        email:'',
        password:'',
    });

    
    const {email, password} = formData;

    const onChange = e => 
        setFormData({...formData,[e.target.name]: e.target.value});
    
    const onSubmit = async e => {
            e.preventDefault();
            props.login(email,password);
    }

    const responseFacebook = async (res) => {
        console.log(res.accessToken);
        await props.oauthFacebook(res.accessToken);
      }

    if(props.isAuthenticated){
        return <Redirect to="/home" />;
    }
    
        return(

         <Fragment>
             <div className={"divStyle"}>
                 <Row className="justify-content-md-center">
                     <Col xs lg="2">
                     </Col>
                     <Col md="auto"><div className="div-reg">
                         <h4 className="card-title mt-3 text-center">Se Connecter</h4>
                         <Form onSubmit={e=> onSubmit(e)}>

                             <Form.Group  controlId="formGridEmail">
                                 <Form.Label>Email</Form.Label>
                                 <Form.Control type="email" placeholder="Enter email"
                                               name="email"
                                               value={email}
                                               onChange ={e => onChange(e)}/>
                             </Form.Group>

                             <Form.Group  controlId="formGridPassword">
                                 <Form.Label>Password</Form.Label>
                                 <Form.Control type="password" placeholder="Password"
                                               name="password"
                                               value={password}
                                               onChange ={e => onChange(e)} />
                             </Form.Group>


                             <Button variant="success" size="lg" type="submit" block>
                                 Sign in
                             </Button>
                             <p className="text-center">Or</p>
                             <FacebookLogin
                                 appId="2260756780842939"
                                 textButton="Login with Facebook"
                                 fields="name,email"
                                 cssClass="loginBtn loginBtn--facebook"
                                 callback={responseFacebook} />
                         </Form>
                     </div></Col>
                     <Col xs lg="2">
                     </Col>
                 </Row>
             </div>
         </Fragment> 
            
        );
    }

const mapStateTopProps = state => ({
        isAuthenticated: state.auth.isAuthenticated
});
export default connect(mapStateTopProps,{ setAlert , login, oauthFacebook })(Login);