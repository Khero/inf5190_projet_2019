import React, { Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { deleteFavorite } from "../../Actions/profile";
import {Row, Button, Table} from "react-bootstrap";
import './Favorite.css'

const Favorite = props => {
  const favoris = props.favorite_places.map(fav => (
    <tr key={fav._id}>
      <td>{fav.name}</td>
      <td>
        <Button variant="outline-danger" onClick={e => props.deleteFavorite(fav._id)}>
          Supprimer
        </Button>
      </td>
    </tr>
  ));

  return (
    <Fragment>
      <div >
        <br />
        <br />
        <br />
        <Row>
          <h6>Liste des Favoris</h6>
        </Row>
        <Table responsive striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Nom du jardin</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{favoris}</tbody>
        </Table>
      </div>
    </Fragment>
  );
};

Favorite.propTypes = {
  favorite_places: PropTypes.array.isRequired,
  deleteFavorite: PropTypes.func.isRequired
};

export default connect(
  null,
  { deleteFavorite }
)(Favorite);
