import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { getCommentForPlace, deleteComment } from "../../Actions/comment";
import PropTypes from "prop-types";
import {Row, Col, Button } from "react-bootstrap";
import Moment from 'react-moment'

const ItemComment = ({
  auth,
  comments,
  critic,
  getCommentForPlace,
  deleteComment
}) => {
  useEffect(() => {
    getCommentForPlace(critic);
  }, [getCommentForPlace, critic, comments.loading]);

  return (
    <Fragment>
      <div>
        {comments.comment &&
          comments.comment.map(user => {
            return user.map(item => (
              <div key={item._id}>
                <br />
                <Row>
                  <Col>
                    <b>{item.user_name}</b>
                  </Col>
                  <Col sm={{offset: 2}}>
                    {auth._id === item.user_id && (
                        <Button variant="danger" onClick={e => deleteComment(critic, item._id)}>

                          Supprimer
                      </Button>
                    )}
                  </Col>
                </Row>
                <Row className="dateField">
                  <Col><Moment format='DD/MM/YYYY'>{item.date}</Moment></Col>
                </Row>

                <Row className="commentField">
                  <Col>"{item.text}"</Col>
                </Row>
              </div>
            ));
          })}
      </div>
    </Fragment>
  );
};

ItemComment.propTypes = {
  critic: PropTypes.string.isRequired,
  auth: PropTypes.object.isRequired,
  getCommentForPlace: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth.user,
  comments: state.comments
});
export default connect(
  mapStateToProps,
  { getCommentForPlace, deleteComment }
)(ItemComment);
