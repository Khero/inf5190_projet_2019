import React,  { useState, useEffect, Fragment} from 'react';
import { connect } from 'react-redux';
import { addComment} from '../../Actions/comment';
import PropTypes from 'prop-types';
import { Button, Form} from "react-bootstrap";
import ItemComment from './ItemComment';


const Comments = (props)=>{
    const[formData , setFormData] = useState(
        {
            text: '',

        },
    );


    const [infoData, setInfoData] =useState ({
            user_id: '',
            user_name: '',
            place_id: '',
            place_name: '',
    })

    

        useEffect(()=>{
            const initInfo = () =>{
                props.auth && props.auth.method[0] === "local" ?
                setInfoData({
                    place_id: props.selectedGarden.garden._id ,
                    place_name: props.selectedGarden.garden.Text_,
                    user_id: props.auth._id,
                    user_name: props.auth.local.name})
                    :
                    setInfoData({
                        place_id: props.selectedGarden.garden._id ,
                        place_name: props.selectedGarden.garden.Text_,
                        user_id: props.auth._id,
                        user_name: props.auth.facebook.name})

            };
            initInfo();
        },[props]);

       
    

    const onChange = e =>{
        setFormData({...formData,[e.target.name]: e.target.value});
        }
    
    const onSubmitComment = e => {
        e.preventDefault();
        //console.log(infoData,formData);
        props.addComment(infoData,formData);
        
       
    }

    return(
        <Fragment>
        <Form onSubmit={(e =>onSubmitComment(e))}>
            <Form.Group controlId="Comments">
                <Form.Label>Commentaires</Form.Label>
               
                <Form.Control
                type="comment"
                placeholder="Comment here"
                name="text"
                value={formData.text}
                onChange ={e => onChange(e)}
                />
               
            </Form.Group>
            <Button variant="primary" type="submit">
                    Ajouter Commentaires
                </Button>
        </Form>
        <div>
        <ItemComment  critic={infoData.place_id}/>
    
        
        </div>
    </Fragment>
        
    )   
}



Comments.protoType ={
    addComment:PropTypes.func.isRequired,
    getNumReservation:PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    auth: state.auth.user,
    comments:state.comments
});
export default connect(mapStateToProps, {addComment})(Comments);