
import React, { Component }from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps"

const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={8}
    zoom={props.zoom}
    defaultCenter={{ lat: -34.397, lng: 150.644 }}
    center={props.center}
    options={{gestureHandling:'greedy'}}
  >
    {props.markers && props.markers.map((point) => ( 
        <Marker key={point.garden.OBJECTID} position={{ 
        lat: point.garden.geometry.coordinates[1],
        lng: point.garden.geometry.coordinates[0] }} 
        onClick={() =>{ props.handleMarkerClick(point)}}
        icon={{
          url: '/garden.png',
          scaledSize: new window.google.maps.Size(35,35)
        }}
        >
          {point.isOpen && <InfoWindow
          onCloseClick={()=>{
            props.handleWindowCloseClick();
          }}>
            <div>
              <p>Jardin communautaire {point.garden.Text_}</p>
              <p>Adresse: {props.address}</p>
              <p>Nombre de bacs : {point.garden.Nbacs}</p>
              <p>Nombre de reservations: {props.reservationNum}</p>
            </div>
          </InfoWindow>}
        </Marker>

        
       
    ))}


    {props.bixiNear && props.bixiNear.map((point) => ( 
        <Marker key={point.bixi.station_id} position={{ 
        lat: point.bixi.geometry.coordinates[1],
        lng: point.bixi.geometry.coordinates[0] }} 
        onClick={() =>{props.handleBixiClick(point)}}
        icon={{
          url: '/bixi.png',
          scaledSize: new window.google.maps.Size(35,35)
        }}
        >
          {point.isOpen && <InfoWindow>
            <div>
              <p>Nom: {point.bixi.name}</p>
              <p>Distance: {parseInt(point.bixi.distance)} mètres</p>
              <p>Docks disponnible: {point.bixi.num_docks_available}</p>
              <p>Vélo disponnible: {point.bixi.num_bikes_available}</p>
            </div>
          </InfoWindow>}
        </Marker>
    ))}

      {props.busNear && props.busNear.map((point) => ( 
          <Marker key={point.station.stop_id} position={{ 
          lat: point.station.geometry.coordinates[1],
          lng: point.station.geometry.coordinates[0] }} 
          onClick={() =>{props.handleBusClick(point)}}
          icon={{
            url: '/stm.png',
            scaledSize: new window.google.maps.Size(35,35)
          }}
          >
            {point.isOpen && <InfoWindow>
              <div>
                <p>Nom de l'arrêt: {point.station.stop_name}</p>
                <p>Code de l'arrêt {point.station.stop_code}</p>
                <p>Distance: {parseInt(point.station.distance)} mètres</p>
              </div>
            </InfoWindow>}
          </Marker>
      ))}


  </GoogleMap>
))

export default class Map extends Component {
  render(){
    return(
        <MyMapComponent       
          {...this.props}
          isMarkerShown
          googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyBfcsOTkFuWLWmOjlgFkAk0OYjQy-pDO0g"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `100%` , width:`100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        />
    );
  }

}
