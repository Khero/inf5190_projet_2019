import React, { Component } from 'react';
import Geocode from "react-geocode";
import { connect } from 'react-redux';
import { loadGardens} from '../../Actions/loadDB';
import PropTypes from 'prop-types';
import axios from 'axios';
import './Map.css';
import Sidebar from './sidebar';
import Map from './Map';



class MapContainer extends Component {
    constructor(){
      super();
      this.state={
        center:{ lat: 45.50169, lng: -73.567253 },
        zoom:11,
        markers:[],
        bixiNear:[],
        busNear:[],
        selectedGarden:{},
        address:'',
        gardenNear:[],
        reservationNum:0
      }
    }



    handleSearch = (string) =>{
      Geocode.fromAddress(string).then(
        response => {
          const { lat, lng } =  response.results[0].geometry.location;
         
          this.gardenArea(lng,lat);
        },
        error => {
          console.error(error);
        }
      );
    

    }




  getAdress = (lat,lon) =>{
      Geocode.fromLatLng(lat,lon).then(
        response => {
          const address = response.results[0].formatted_address;
          this.setState({address: address});
         
        },
        error => {
          console.error(error);
        }
      );

    }


    getNumReservation = async (place_id) => {
      const config = {
          headers: {
              'Content-Type': 'application/json'
          }
      }
      const body = JSON.stringify({place_id: place_id});

          await axios.post('http://localhost:4250/api/reservation/sum', body, config).then((res)=>{
            if(res.data[0]){
              this.setState({reservationNum: res.data[0].reservations});
            }else {
             
              this.setState({reservationNum: 0});
            }
            
          }).catch((err) => {
            console.log(err);
          });  

  }

  //Get all the bixi 250 M
  gardenArea = async (lon,lat) =>{
    const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  }
  const body = {
              lon: parseFloat(lon),
              lat: parseFloat(lat)
          };
      await axios.post('http://localhost:4100/api/garden/nearPoint', body, config).then((res)=>{
        if(res.data.length>0){
          const garden = {
            garden: res.data[0],
            isOpen:false,
          }
          this.setState({
            gardenNear: res.data,
            center: {
              lng: res.data[0].geometry.coordinates[0],
              lat: res.data[0].geometry.coordinates[1]   
            },
            zoom: 17,
            selectedGarden:{
              garden: res.data[0]
            }, 
          });
          this.getAdress(res.data[0].geometry.coordinates[1],res.data[0].geometry.coordinates[0]);
          this.handleInfoWindowSearchbar(garden); 
        }
        
        

          
          
      }).catch((err) => {
        console.log(err);
      });   
  }

    //Get all the bixi 250 M
  bixiArea = async (lon,lat) =>{
    const config = {
      headers: {
          'Content-Type': 'application/json'
      }
  }
  const body = {
              lon: parseFloat(lon),
              lat: parseFloat(lat)
          };
      await axios.post('http://localhost:4000/api/bixi/nearPoint', body, config).then((res)=>{
          const bixiFormat= res.data.map(bixi => {
            return{
                    bixi: bixi,
                    isOpen:false
                  }
          });
          this.setState({bixiNear: bixiFormat});
      }).catch((err) => {
        console.log(err);
      });   
  }

  //Get all the bus 250 M
  busArea = async (lon,lat) =>{
    const config = {
      headers: {
          'Content-Type': 'application/json'
      }
    }
    const body = {
              lon: parseFloat(lon),
              lat: parseFloat(lat)
          };
      await axios.post('http://localhost:4050/api/bus/nearPoint', body, config).then((res)=>{
        const busFormat = res.data.map(station=> {
          return{
                  station: station,
                  isOpen:false
                }
        });
        this.setState({busNear: busFormat});
      }).catch((err) => {
        console.log(err);
      });
    }



    handleInfoWindowSearchbar =  garden => {
        const marker = this.state.markers.find(marker => marker.garden._id === garden.garden._id);
        this.handleMarkerClick(marker);

    }

    handleMarkerClick = (marker) => {
        this.closeAllMarkers();
        this.bixiArea(marker.garden.geometry.coordinates[0],marker.garden.geometry.coordinates[1]);
        this.busArea(marker.garden.geometry.coordinates[0],marker.garden.geometry.coordinates[1]);
        this.getAdress(marker.garden.geometry.coordinates[1],marker.garden.geometry.coordinates[0]);
        this.setState(Object.assign(this.state.markers,marker));
        this.setState({
                        selectedGarden:marker
                      });
        this.getNumReservation(marker.garden._id);
        marker.isOpen = true;
        //console.log (marker);
    }




    handleBusClick = (bus) => {
      this.closeAllStation();
      
      bus.isOpen =true;
      this.setState(Object.assign(this.state.busNear,bus));
  }




    handleBixiClick = (bixi) => {
      this.closeAllStation();
      bixi.isOpen =true;
      this.setState(Object.assign(this.state.bixiNear,bixi));
  }


    closeAllStation = () =>{

      const bixis = this.state.bixiNear.map(bixi =>{
        bixi.isOpen =false;
        return bixi;
      });

      const stations = this.state.busNear.map(bus =>{
        bus.isOpen =false;
        return bus;
      });
      this.setState(Object.assign(this.state.busNear,stations));
      this.setState(Object.assign(this.state.bixiNear,bixis),);



    }

    handleWindowCloseClick = () => {
      this.setState({bixiNear:[],busNear:[],selectedGarden:{}});
      
    }

    closeAllMarkers = ()=>{
      const markers = this.state.markers.map(marker =>{
        marker.isOpen =false;
        return marker;
      });
      this.setState(Object.assign(this.state.markers,markers));
    }


    componentWillMount(){
      this.props.loadGardens().then(()=> {
        this.formatGarden();
        Geocode.setApiKey("AIzaSyBfcsOTkFuWLWmOjlgFkAk0OYjQy-pDO0g");
      })
    
    }
    



    formatGarden(){
      const marker = this.props.loadDB.garden.map((point)=>{
        return {
          garden:point,
          isOpen:false,
        }
      })
      this.setState({markers:marker});

    }
  

  render() {
    return (
        
      <div id='App'>
        <Sidebar {...this.state}
          handleSearch={this.handleSearch}/>

        <Map {...this.state}  
              handleMarkerClick={this.handleMarkerClick} 
              handleWindowCloseClick={this.handleWindowCloseClick} 
              handleBixiClick={this.handleBixiClick}
              handleBusClick={this.handleBusClick}
        />
      </div>
    );
  }
}


MapContainer.protoTypes = {
  loadGardens: PropTypes.func.isRequired,
  loadDB:PropTypes.object.isRequired,
  auth:PropTypes.object.isRequired
}

const mapStateTopProps = state => ({
  auth: state.auth,
  loadDB: state.loadDB
});
export default connect(mapStateTopProps ,{ loadGardens })(MapContainer);