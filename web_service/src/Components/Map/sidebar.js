import React, { Component, Fragment } from "react";
import { Button, Form, Accordion, Card} from "react-bootstrap";
import "./sidebar.css";

//import { slide as Menu } from 'react-burger-menu'
import Comments from '../Comments/Comment'
import { connect } from 'react-redux';
import { getAllComments } from '../../Actions/comment';

import { addFavorite } from '../../Actions/profile';
//import Favorite from '../Favorite/Favorite'
import PropTypes from 'prop-types';
//import Foo from "../Layout/StartRate";
import Reservation from "../Reservation/Reservation";
import Bail from "../Bail/Bail";



class SideBar extends Component {

    constructor(){
        super();
        this.state={
            SearchBar:'',
        }
    }

   componentWillMount(){
     this.props.getAllComments()


   }
 
  changeViewSideBar() {
    this.setState({
      view: !this.state.view,
      viewButton: !this.state.viewButton,
      heart: !this.state.heart
    });
  }

  changeViewField() {
    this.setState({ viewField: !this.state.viewField });
  }

  
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }
  onSubmitFormSearch = e => {
      e.preventDefault();
    this.props.handleSearch(this.state.SearchBar);
     
  }

  handleSearchBarChange = e => {
      this.setState({[e.target.name]: e.target.value});

  }

  render() {
    let sidebar_class = this.state.view ? "sidebarOpen" : "sidebarClose";
    let collapseButton = this.state.viewButton
      ? "collapseOpen"
      : "collapseClose";

    return (
      <Fragment>


       <div
          id="collapseButton"
          className={collapseButton}
          onClick={this.changeViewSideBar.bind(this)}
        >
          {collapseButton === "collapseOpen" ? <div>Fermer</div> : <div>Ouvrir</div>}
        </div>

        <div id="sidebar" className={sidebar_class}>
          <div id="sidebarSection">
            <Form onSubmit={e => this.onSubmitFormSearch(e)}>
                <Form.Group controlId="formGridZip">
                    <Form.Control type="Text" placeholder="Chercher ici" 
                        name="SearchBar"
                        value={this.state.SearchBar}
                        onChange ={e => this.handleSearchBarChange(e)}
                        required
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Chercher
                </Button>
            </Form>
          </div>
        
          {this.props.selectedGarden.garden && 
          <div id="sidebarSection">
            <h3 id="header">Jardin Communautaire {
                this.props.selectedGarden.garden
                && this.props.selectedGarden.garden.Text_ }
            </h3>
                <div>
                    <h6>Lots disponnibles:</h6>
                    <p>{this.props.selectedGarden.garden.Nbacs }</p>
                    {this.props.address &&
                        <div> 
                            <h6><img src="https://img.icons8.com/color/48/000000/address.png" alt="marker icone"></img></h6>
                            <p>{this.props.address}</p>
                        </div>
                    }
                </div>
                        <div>
                          <Button variant="primary" onClick={e => this.props.addFavorite(this.props.selectedGarden.garden.Text_,
                            this.props.selectedGarden.garden._id)}>
                              Ajouter aux favoris
                          </Button >
                        
                    </div>


                    
                
          </div>}
        
          {this.props.selectedGarden.garden && 
                <div id="divSidebarSectionBorder">
                    <Accordion>
                    <Card>
                        <Card.Header>
                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                            Commentaires 
                        </Accordion.Toggle>
                        </Card.Header>
                        <Accordion.Collapse eventKey="0">
                        <Card.Body>
                           
                            <Comments {...this.props}/>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    </Accordion>
                </div>
          }
          {this.props.selectedGarden.garden && 
            this.props.selectedGarden.garden.Nbacs > 0 &&
                        <div id="divSidebarSectionBorder">
                            <Accordion>
                            <Card>
                                <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    Signer un bail
                                </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Bail {...this.props} />
                                </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            </Accordion>
                        </div>
                }
                
                {this.props.selectedGarden.garden && 
                    parseFloat(this.props.selectedGarden.garden.Nbacs) === 0 &&

                        <div id="divSidebarSectionBorder">
                            <Accordion>
                            <Card>
                                <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                    Réservation
                                </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Reservation {...this.props}/>
                                </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            </Accordion>
                        </div>
                    }

        </div>
      </Fragment>
    );
  }
}

SideBar.protoType = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  getNumReservation:PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  profile: state.profile.profile,
  auth:state.auth
});
export default connect(mapStateToProps, { getAllComments , addFavorite})(SideBar);
