import React from 'react';
import { connect } from 'react-redux';
import PropTypes from  'prop-types';
import Slideshow from 'react-slidez'
import { Spinner } from 'react-bootstrap';

const Dashboard = ({ auth, profile:{ profile, loading} }) =>{

    return loading && profile === null ?
        <div>
            Chargement
            <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        </div>
        :

    <Slideshow
        autoplay
        enableKeyboard
        useDotIndex
        slideInterval={10000}
        defaultIndex={1}
        slides={['slideshow/1.jpg', 'slideshow/2.jpg', 'slideshow/3.jpg', 'slideshow/4.jpg', 'slideshow/5.jpg', 'slideshow/6.jpg']}
        effect={'left'}
        height={'94.2%'}
        width={'100%'}
    />



}

Dashboard.propTypes = {
    auth: PropTypes.object.isRequired,
    profile: PropTypes.object.isRequired
}
const mapStateToProps = state => ({
    auth: state.auth,
    profile: state.profile
});
export default connect (mapStateToProps, { })(Dashboard);