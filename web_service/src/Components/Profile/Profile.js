import React, { Fragment, useState, useEffect } from "react";
import {
  Button,
  Form,
  ButtonToolbar,
  Container,
  Row,
  Col,
  Modal
} from "react-bootstrap";
import { connect } from "react-redux";
//import { Link , withRouter } from 'react-router-dom';
import {
  getCurrentProfile,
  updateProfile,
  deleteAccountProfile
} from "../../Actions/profile";
import PropTypes from "prop-types";
import Favorite from "../Favorite/Favorite";
import "./profile.css"

function MyModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">Alerte!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>Effacer votre compte</h4>
        <p>
          Etes-vous certain que vous voulez supprimer votre compte ? ATTENTION
          CETTE ACTION N’EST PAS REVERSIBLE.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Cancel</Button>
        <Button onClick={e => props.deleteaccount(e)}>Supprimer</Button>
      </Modal.Footer>
    </Modal>
  );
}

const Profile = ({
  getCurrentProfile,
  updateProfile,
  profile,
  auth,
  deleteAccountProfile
}) => {
  const [modalShow, setModalShow] = React.useState(false);

  const [formData, setFormData] = useState({
    name: "",
    lastname: "",
    email: "",
    phone: ""
  });

  useEffect(() => {
    !auth.loading && auth.user.method[0] === "local"
      ? setFormData({
          name:
            profile.loading || !profile.profile.user.local.name
              ? ""
              : profile.profile.user.local.name,
          lastname:
            profile.loading || !profile.profile.user.local.name
              ? ""
              : profile.profile.user.local.name,
          email:
            profile.loading || !profile.profile.user.local.email
              ? ""
              : profile.profile.user.local.email,
          phone:
            profile.loading || !profile.profile.phone
              ? ""
              : profile.profile.phone
        })
      : setFormData({
          name:
            profile.loading || !profile.profile.user.facebook.name
              ? ""
              : profile.profile.user.facebook.name,
          lastname:
            profile.loading || !profile.profile.user.facebook.name
              ? ""
              : profile.profile.user.facebook.name,
          email:
            profile.loading || !profile.profile.user.facebook.email
              ? ""
              : profile.profile.user.facebook.email,
          phone:
            profile.loading || !profile.profile.phone
              ? ""
              : profile.profile.phone
        });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getCurrentProfile, profile.loading, auth.loading]);

  const onSubmit = async e => {
    e.preventDefault();
    await updateProfile(formData);
    getCurrentProfile();
  };

  const { phone, name, email } = formData;

  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onClick = e => {
    e.preventDefault();
    deleteAccountProfile();
  };

  return (
      <div className={"divStyle3"}>
    <Container>
      <Fragment>

        <Row className="justify-content-md-center">
          <Col xs lg="2" />
          <Col md="auto">
            <div className={"div-reg3"}>
              <h4 className="card-title mt-3 text-center">Your Profile</h4>
              <Form onSubmit={e => onSubmit(e)}>
                <Form.Group controlId="formGridName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="firstname"
                    placeholder="Enter firstname"
                    name="name"
                    value={name}
                    onChange={e => onChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="formGridEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    name="email"
                    value={email}
                    onChange={e => onChange(e)}
                  />
                </Form.Group>

                <Form.Group controlId="formGridPassword">
                  <Form.Label>Phone</Form.Label>
                  <Form.Control
                    type="phone"
                    placeholder="Ex: (xxx)-xxx-xxxx"
                    name="phone"
                    value={phone}
                    onChange={e => onChange(e)}
                  />
                </Form.Group>
                <ButtonToolbar>
                  <Button variant="success" type="submit">
                    Sauvegarder changement
                  </Button>
                  <Button variant="danger" onClick={() => setModalShow(true)}>
                    Supprimer Compte
                  </Button>
                </ButtonToolbar>
                <MyModal
                  deleteaccount={onClick}
                  show={modalShow}
                  onHide={() => setModalShow(false)}
                />
              </Form>
            </div>
          </Col>
          

          <Col xs lg="2" />
        </Row>

        <Row  className="justify-content-md-center">
        <Col xs lg="2" />
          <Col md="auto">
          <div>
              {profile.profile && (
                <Favorite favorite_places={profile.profile.favorite_places} />
              )}
            </div>
          </Col>
          <Col xs lg="2" />
        </Row>

      </Fragment>
    </Container>
      </div>
  );
};
Profile.propTypes = {
  updateProfile: PropTypes.func.isRequired,
  deleteAccountProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});
export default connect(
  mapStateToProps,
  { getCurrentProfile, updateProfile, deleteAccountProfile }
)(Profile);
