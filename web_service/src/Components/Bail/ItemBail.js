import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getMyBail } from "../../Actions/reservation";
import Moment from "react-moment";
import { Row, Col} from "react-bootstrap";

const smallerPicture = {
  width: "20px",
  height: "auto"
};

const ItemBail = ({ reservation, infoData, getMyBail }) => {
  useEffect(() => {
    getMyBail(infoData.user_id);
  }, [getMyBail, infoData.user_id]);

  return (
    <Fragment>
      <div>
        {reservation.Bail &&
          reservation.Bail.map(reserv => (
            <div key={reserv._id}>
              {reserv.place_id === infoData.place_id &&
                reserv.user_id === infoData.user_id && (
                  <div>
                    <br />
                    <Row>
                      <Col>
                        <img
                          style={smallerPicture}
                          src="https://png.pngtree.com/svg/20170517/dc68cfb49c.svg"
                          alt=''
                        />
                        {reserv.place_name}
                      </Col>
                    </Row>
                    <br />
                    <Row>
                      <Col>Début </Col>
                      <Col>Fin</Col>
                    </Row>

                    <Row>
                      <Col>
                        <Moment className="dateField" format="DD/MM/YYYY">
                          {reserv.date_start}
                        </Moment>
                      </Col>

                      <Col>
                        <Moment className="dateField" format="DD/MM/YYYY">
                          {reserv.date_end}
                        </Moment>
                      </Col>
                    </Row>

                    <p>
                      (note : vous ne pouvez avoir qu'un seule bail actif par
                      jardin)
                    </p>
                  </div>
                )}
            </div>
          ))}
      </div>
    </Fragment>
  );
};

ItemBail.propTypes = {
  infoData: PropTypes.object.isRequired,
  reservation: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth.user,
  reservation: state.reservation
});

export default connect(
  mapStateToProps,
  { getMyBail }
)(ItemBail);
