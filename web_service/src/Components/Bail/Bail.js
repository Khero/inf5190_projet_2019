import React, {Fragment, useEffect , useState} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { signBail } from '../../Actions/reservation'
import ItemBail from './ItemBail';
import {Button, Form, Modal} from "react-bootstrap";

function MyModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Alerte!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4>Signer un Bail</h4>
                <p>
                    Etes-vous certain que vous voulez signer un bail ? ATTENTION
                    CETTE ACTION N’EST PAS REVERSIBLE.
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onHide}>Refuser</Button>
                <Button onClick={e => props.reserve(e)}>Accepter</Button>
            </Modal.Footer>
        </Modal>
    );
}



const Bail = props => {
    const [modalShow, setModalShow] = React.useState(false);
    const [infoData, setInfoData]  = useState ({
        user_id: '',
        user_name: '',  
        place_id: '',
        place_name: '',
    })

    const onSubmitReservation = e => {
        e.preventDefault();
        //console.log(infoData,formData);
       props.signBail(infoData);
       setModalShow(false);
    }


    useEffect(()=>{
        const initInfo = () =>{
            props.auth && props.auth.method[0] === "local" ?
            setInfoData({
                place_id: props.selectedGarden.garden._id ,
                place_name: props.selectedGarden.garden.Text_,
                user_id: props.auth._id,
                user_name: props.auth.local.name})
                :
                setInfoData({
                    place_id: props.selectedGarden.garden._id ,
                    place_name: props.selectedGarden.garden.Text_,
                    user_id: props.auth._id,
                    user_name: props.auth.facebook.name})

        };
        initInfo();
    },[props]);

    return (
        <Fragment>
            <div>
                <Form>
                    <Button variant="primary" onClick={() => setModalShow(true)}>
                        Signer un Bail
                    </Button>
                </Form>
                <ItemBail infoData={infoData} onSubmitReservation={onSubmitReservation}/>
            </div>
            <MyModal
                reserve={e => onSubmitReservation(e)}
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </Fragment>
        
    )
}

Bail.propTypes = {
    auth:PropTypes.object.isRequired,
    signBail:PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    auth: state.auth.user,
});
export default connect(mapStateToProps , {signBail})(Bail)