import { GET_COMMENTS,
         GET_COMMENT,
         ADD_COMMENTS,
         REMOVE_COMMENTS,
         COMMENTS_ERROR
        } from '../Actions/types'


const initialState = {
    comments:[],
    comment: [],
    ratings:[],
    rating:{},
    loading: true,
    error:{}
}


export default function (state = initialState, actions) {
    const { type, payload } = actions;
    
    switch(type) {
        case GET_COMMENTS:
            return{
                ...state,
                comments:payload,
                loading:false
            }
        case GET_COMMENT:
            return{
                ...state,
                comment:payload,
                loading:false
            }
        case COMMENTS_ERROR:
            return{
                ...state,
                error:payload,
                loading:false
            }
        case ADD_COMMENTS:
        case REMOVE_COMMENTS:
            return{
                ...state,
                comment:[payload],
                loading:false
            };
        default:
            return state

    }
}