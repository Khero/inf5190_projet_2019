import { SIGNED_FAIL, SIGNED_SUCCESS, RESERVATION_SUCCESS, RESERVATION_FAIL, GET_RESERVATION,
GET_BAIL, REMOVE_RESERVATION , CLEAR_RESERVATION} from '../Actions/types';

const initialState = {
    Reservations: [],
    Bail: [],
    loading: true,
    
    error: {}
}

export default function( state = initialState, action ) {

    const { type, payload } = action;
    switch(type){
        case REMOVE_RESERVATION:
        case RESERVATION_SUCCESS:
        case GET_RESERVATION:
            return {
                ...state,
                Reservations: payload,
                loading: false
            }
        case SIGNED_SUCCESS:   
        case GET_BAIL:
            return {
                ...state,
                Bail: payload,
                loading: false
            }
        case SIGNED_FAIL:
        case RESERVATION_FAIL:
            return  {
                    ...state,
                    error: payload,
                    loading: false
                }
        case CLEAR_RESERVATION:
            return{
                ...state,
                Reservations: [],
                Bail:[]
            } 
        default : 
            return state
    }
        
}