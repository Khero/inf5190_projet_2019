import { GET_PROFILE, PROFILE_ERROR, CLEAR_PROFILE , ACCOUNT_DELETED, ACCOUNT_DELETE_ERROR, ADD_FAVORITE,
FAVORITE_ERROR, REMOVE_FAVORITE} from '../Actions/types';

const initialState = {
    profile: null,
    loading: true,
    error: {}
}

export default function( state = initialState, action ) {

    const { type, payload } = action;

    switch(type){
        case GET_PROFILE:
        case REMOVE_FAVORITE:
        case ADD_FAVORITE:
            return {
                ...state,
                profile: payload,
                loading: false
            }
        
        case FAVORITE_ERROR:
        case ACCOUNT_DELETE_ERROR:
        case PROFILE_ERROR:
            return {
                ...state,
                error: payload,
                loading: false
            }
        case ACCOUNT_DELETED:  
        case CLEAR_PROFILE:
            return{
            ...state,
            profile: null
        }
        default : 
            return state
    }

}