import { combineReducers} from 'redux';
import alert from './alerts';
import auth from './auth';
import profile from './profile';
import loadDB from './loadDB';
import comments from './comment';
import reservation from './reservation';
import 'bootstrap/dist/css/bootstrap.min.css';


export default combineReducers ({
    alert,
    auth,
    profile,
    loadDB,
    comments,
    reservation
});