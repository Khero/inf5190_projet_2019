import { 
    GARDENS_LOADED ,
    BIXI_LOADED,
    BUS_LOADED,
    GARDENS_LOAD_ERROR,
    BUS_LOAD_ERROR,
    BIXI_LOAD_ERROR
   } from '../Actions/types';


   const initialState = {

    garden:[],
    bus:[],
    bixi:[],
    loading:true,
    error:{}
}

export default function ( state = initialState , action ) {
    const { type , payload } = action;
    switch (type) {
        case GARDENS_LOADED:
            return{
                ...state,
                garden: payload,
                loading: false
            };
        case BIXI_LOADED:
            return{
                ...state,
                bixi: payload,
                loading: false
            };
        case BUS_LOADED:
            return{
                ...state,
                bus: payload,
                loading: false
            };
        case BUS_LOAD_ERROR:
            return {
                ...state,
                error: payload,
                loading: false
            }
        case BIXI_LOAD_ERROR:
            return {
                ...state,
                error: payload,
                loading: false
            }
        case GARDENS_LOAD_ERROR:
            return {
                ...state,
                error: payload,
                loading: false
            }
        default:
            return state
    }
}