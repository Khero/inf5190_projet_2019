import React from 'react';
import classes from './myModal.module.css';


const myModal = props => {
    return (
        <div className={classes.Modal}>
            {props.children}
        </div>
    );
};


export default myModal;