cd services/Bixi_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../Bus_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../Critics_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../Garden_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../User_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../Reservation_Microservice
Docker-compose build
Docker-compose up -d
Docker-compose start

cd ../../web_service
Docker-compose build
Docker-compose up -d
Docker-compose start
PAUSE