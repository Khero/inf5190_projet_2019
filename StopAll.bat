cd services/Bixi_Microservice
Docker-compose stop
Docker-compose down

cd ../Bus_Microservice
Docker-compose stop
Docker-compose down

cd ../Critics_Microservice
Docker-compose stop
Docker-compose down

cd ../Garden_Microservice
Docker-compose stop
Docker-compose down

cd ../User_Microservice
Docker-compose stop
Docker-compose down

cd ../../web_service
Docker-compose stop
Docker-compose down
PAUSE


cd services/Critics_Microservice
Docker-compose stop
Docker-compose down