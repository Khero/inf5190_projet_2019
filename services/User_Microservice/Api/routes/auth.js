const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const authController = require('../controllers/AuthController.js');
const passport = require('passport');

//Initialize passport configuration
const passportConf = require('../../config/passport');

// Passport auth
const passportAuth = passport.authenticate('local', { session: false });
const passJwt = passport.authenticate('jwt', { session: false });
const passFacebook = passport.authenticate('facebookToken', { session: false });


router.post('/', [
    check('email', 'Invalid email').isEmail(),
    check('password', 'Invalid password').exists()
],passportAuth,authController.signInUser);


router.get('/',passJwt,authController.getUser);

router.post('/oauth/facebook',passFacebook,authController.oauthFacebook);

module.exports = router;