const express = require('express');
const router = express.Router();
const passport = require('passport');
const profileController = require('../controllers/ProfileController.js');

//Initialize passport configuration
const passportConf = require('../../config/passport');
//Passport authentification
const passJwt = passport.authenticate('jwt', { session: false });

// @route   GET api/profile/authUser
// @desc    Get the authenticated user profile
// @access  Private
router.get('/authUser', passJwt , profileController.getUserProfile);


// @route   POST api/profile
// @desc    Create and update profile for the authenticated user
// @access  Private
router.post('/', passJwt , profileController.createUpdateProfile);


// @route   DELETE api/profile
// @desc    Delete profile & user
// @access  Private
router.delete('/', passJwt , profileController.deleteProfileUser);


// @route   PUT api/favorite
// @desc    Add to profile a favorite place
// @access  Private
router.put('/favorite', passJwt , profileController.addFavorite);


// @route   Delete api/favorite
// @desc    Delete from profile a favorite place
// @access  Private
router.delete('/favorite/:place_id', passJwt , profileController.deleteFavorite);




module.exports = router;