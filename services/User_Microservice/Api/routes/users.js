const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const userController = require('../controllers/UserController');


router.post('/', [
    check('local.name', 'Name is required').not().isEmpty(),
    check('local.email', 'Invalid email').isEmail(),
    check('local.password', 'Invalid password').isLength({ min : 6}),
],userController.registerUser);


module.exports = router;