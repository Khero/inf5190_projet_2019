const Profile = require('../../models/Profile');
const User = require('../../models/User');



exports.getUserProfile = async (req,res) => {

    try {
        let profile = await Profile.findOne({ user: req.user.id }).populate('user', ['local.name','local.email','facebook.name','facebook.email']);
        if(!profile){

            const profileFields = {};
            profileFields.user = req.user.id;
            profile = new Profile(profileFields);
            await profile.save();
            profile = await Profile.findOne({ user: req.user.id }).populate('user', ['local.name','local.email','facebook.name','facebook.email']);
            
        }
        res.json(profile);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !');
    }
}

exports.createUpdateProfile = async (req,res) => {

    const { name, email, phone } = req.body;

    //Build profile object

    const profileFields = {};

    profileFields.user = req.user.id;
    if(phone) profileFields.phone = phone;
    

    try {
        let profile = await Profile.findOne({ user: req.user.id });

        //If we find profile we update it
        if(profile){

            if(name){
                
                let user = await User.findByIdAndUpdate(
                    req.user.id , 
                    { $set: {"local.name": name ,
                             "facebook.name": name
                            }
                    },
                    { new: true }
                );

            };

            if(email){
                let user = await User.findByIdAndUpdate(
                    req.user.id  , 
                    { $set: {"local.email": email,
                             "facebook.email": email
                            }
                    },
                    { new: true }
                );
            }
             profile = await Profile.findOneAndUpdate(
                { user: req.user.id } , 
                { $set: profileFields},
                { new: true }
            );
            return res.json(profile);
        }
        //If we don't find it , we create it 
        profile = new Profile(profileFields);
        await profile.save();
        res.json(profile);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !');  
    }
    console.log(req.user.id);
    res.send('works');
}

exports.deleteProfileUser = async (req,res) => {

    try {
        //remove profile
        await Profile.findOneAndRemove({ user: req.user.id });
        //remove user
        await User.findOneAndRemove({ _id: req.user.id });

        res.json({msg: 'User deleted'});
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !');
    }
}

exports.addFavorite = async (req,res) => { 
    
    const {
        name,
        DB_ID
    } = req.body;

    const newFavPlace = {
        name,
        DB_ID
    }
   
    try {

        let profile = await Profile.findOne({ user: req.user.id });
        let found = await profile.favorite_places.map(place => place.DB_ID).indexOf(req.body.DB_ID);
        if(found == -1){
            profile.favorite_places.unshift(newFavPlace);
            await profile.save();
        }

            
            res.json(profile);
       
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !');
    }

}

exports.deleteFavorite = async (req,res) => { 

    
    try {

        const profile = await Profile.findOne({ user: req.user.id });
        //Get the remove index
        const removeIndex = await profile.favorite_places.map(place => place.id).indexOf(req.params.place_id);
        
        //If we find the index we remove it
        if(removeIndex != -1){
            profile.favorite_places.splice(removeIndex,1);
            await profile.save();
        }
        
        res.json(profile);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !');
    }


}