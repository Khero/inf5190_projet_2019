const { validationResult } = require('express-validator');
const User = require('../../models/User');
const jwt = require('jsonwebtoken');
const config = require('config');


exports.signInUser = async(req, res) => {

    const errors = validationResult(req);
    //check result of validation
    if(!errors.isEmpty()) {
        return res.status(400).json( { errors : errors.array() })
    }
    

    // decompose the body
    const { email } = req.body;
 
    try{

        //See if user exist
        let user = await User.findOne({ "local.email":email });
        
        if(!user) {
           return res.status(400).json({ errors : [{ msg: 'Invalid credentials'}] });
        }
        
        //Json webtoken to identifie user 
        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(payload, 
                        config.get('jwtToken'),
                        {expiresIn: 360000},
                        (err, token) => {
                            if(err) throw err;
                            res.status(200).json({ token });
                        });

    }catch(err){
        console.error(err.message);
        res.status(500).send('Oups server error !!')
    }
 


}

exports.oauthFacebook = async (req,res) =>{
    try {
    //console.log('req.user',req.user.id);
    const payload = {
        user: {
            id: req.user.id
        }
    }

    jwt.sign(payload, 
                    config.get('jwtToken'),
                    {expiresIn: 360000},
                    (err, token) => {
                        if(err) throw err;
                        res.status(200).json({ token });
                    });
    } catch(err){
        console.error(err.message);
        res.status(500).send('Oups server error !!')
    }
    


}

exports.getUser = async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-local.password');
        res.json(user);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!')
    }
}