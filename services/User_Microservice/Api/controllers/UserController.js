const { validationResult } = require('express-validator');
const User = require('../../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');


exports.registerUser = async(req, res) => {
    
    const errors = validationResult(req);
    //check result of validation
    if(!errors.isEmpty()) {
        return res.status(400).json( { errors : errors.array() })
    }
    
    // decompose the body
    const {name, email, password} = req.body.local; 

    try{

        //See if user exist
        let user = await User.findOne({ "local.name":name });
        let mail = await User.findOne({ "local.email": email });

        if(user || mail) {
           return res.status(400).json({ errors : [{ msg: 'User already exist'}] });
        }
        
        //create a new user
        user = new User({
            method: ['local'],
            local:{
                name,
                email,
                password
            }    
        });

        
        //Encrypt password
        const salt = await bcrypt.genSalt(10);
        user.local.password = await bcrypt.hash(password, salt);
        
        // push the user in the database
        await user.save();


        
        //Json webtoken to identifie user
        
        const payload = {
            user: {
                id: user.id
            }
        }

        jwt.sign(payload, 
                        config.get('jwtToken'),
                        {expiresIn: 360000},
                        (err, token) => {
                            if(err) throw err;
                            res.status(200).json({ token });
                        });

        console.log("User created :" , user);
    }catch(err){
        console.log(req.body.local.name);
        console.error(err.message);
        res.status(500).send('Oups server error !!')
    }
    //console.log(req.body);
    //res.send('Users route');


}