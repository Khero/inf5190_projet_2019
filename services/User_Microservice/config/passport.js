const passport = require ('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt  = require('passport-jwt').ExtractJwt;
const config = require('config');
const User = require('../models/User');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const FacebookTokenStrategy = require('passport-facebook-token');

//JSON WEB TOKENS STRATEGY
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.get('jwtToken')
}, async (payload, done) => {

    try {
        // Find the user specified in token
        const user = await User.findById(payload.user.id);
        // If doesn't exist, handle it.
        if(!user){
            return done(null,false)
        }

        // Otherwhise, return the user.
        done(null,user);
        
    } catch (error) {
        done(error,false);
    }

}));

// LOCAL STRATEGY
passport.use(new LocalStrategy({
    usernameField: 'email'
  }, async (email, password, done) => {
    try {
      // Find the user given the email
      const user = await User.findOne({ "local.email":email });
      
  
      
      // If not, handle it
      if (!user) {
        return done(null, false);
      }
      
      //console.log(email);
      // Check if the password is correct

      const isMatch = await bcrypt.compare(password,user.local.password);
      
      // If not, handle it

      if (!isMatch) {
        return done(null, false);
      }
    
      // Otherwise, return the user
      done(null, user);

    } catch(error) {
      done(error, false);
    }
  }));



  //Facebook Strategy

passport.use('facebookToken', new FacebookTokenStrategy({
  clientID: '2260756780842939',
  clientSecret: 'aa7713e4c077ae235f97bcc4a5ec1fce',
  passReqToCallback: true
}, async (req, accessToken, refreshToken, profile, done) => {
  try {
    //console.log('profile', profile);
    //console.log('accessToken', accessToken);
    //console.log('refreshToken', refreshToken);
    
    if (req.user) {
      // We're already logged in, time for linking account!
      // Add Facebook's data to an existing account
      req.user.method.push('facebook');
      req.user.facebook = {
        id: profile.id,
        name: profile.displayName,
        email: profile.emails[0].value
      }
      await req.user.save();
      return done(null, req.user);
    } else {
      // We're in the account creation process
      let existingUser = await User.findOne({ "facebook.id": profile.id });
      
      if (existingUser) {
        return done(null, existingUser);
      }

      // Check if we have someone with the same email
      existingUser = await User.findOne({ "local.email": profile.emails[0].value })
      
      if (existingUser) {
        // We want to merge facebook's data with local auth
       // existingUser.local.update({name: profile.displayName});
        existingUser.method.push('facebook')
        existingUser.facebook = {
          id: profile.id,
          name: profile.displayName,
          email: profile.emails[0].value
        }
        await existingUser.save()
        return done(null, existingUser);
      }

      const newUser = new User({
        method: ['facebook'],
        facebook: {
          id: profile.id,
          name: profile.displayName,
          email: profile.emails[0].value
        }
      });

      await newUser.save();
      done(null, newUser);
    }
  } catch(error) {
    done(error, false, error.message);
  }
}));