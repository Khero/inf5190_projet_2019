# User_Microservice

This service contains all the users information to login.

## Docker

To start this service using Docker manually, use the following commands : 

* docker-compose build
* docker-compose up
* docker-compose start

Here is a list of useful Docker commands to run or stop the service :

* docker-compose stop (stops the service)
* docker-compose down (unloads the container)

The Docker container is opening up to port **5000**

## Dependencies
Node Js Express
* npm install --silent

Swagger
* npm install swagger-jsdoc
* npm install swagger-ui-express