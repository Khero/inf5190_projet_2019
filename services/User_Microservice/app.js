
const express = require('express');
const connectDB = require ('./config/db');
const app = express();
var cors = require('cors')
swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger/usersApi.json');

//Connect Database
connectDB();

//init Middleware
app.use(cors());
app.use(express.json({ extended : false }));



app.get('/',(req,res) => res.send('API EN ROUTE'));


// Define Routes

app.use('/api/users', require('./Api/routes/users'));
app.use('/api/auth', require('./Api/routes/auth'));
app.use('/api/profile', require('./Api/routes/profile'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server for User_Microservice started on port ${PORT}`));