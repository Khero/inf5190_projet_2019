const mongoose = require ('mongoose');

const UserSchema = new mongoose.Schema({
    method: {
      type: [String],
      emum: ['local', 'facebook'], 
      required: true
    },

    local:{
        name: {
            type: String,
        },
    
        email: {   
            type: String,
            lowercase: true 
        },
    
        password: {
            type: String,
            
        },
    },

    facebook:{

        id:{
            type: String
        },

        name:{
            type: String
        },

        email: {   
            type: String,
            lowercase: true 
        },
    }
    
});

module.exports = User = mongoose.model('user', UserSchema);