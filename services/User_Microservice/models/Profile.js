const mongoose = require ('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },

    phone: {
        type: String,
    },

    favorite_places: [
    {
       name: {
            type: String,
            required: true
        }, 

        DB_ID: {
            type: String,
            required: true
        }
    }
    ]
})

module.exports = Profile = mongoose.model('profile',ProfileSchema);