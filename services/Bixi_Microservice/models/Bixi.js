const mongoose = require ('mongoose');



const GeoSchema = new mongoose.Schema({
    
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
      
})
const BixiSchema = new mongoose.Schema({
    
            station_id: {
                type: String
            },
    
            name : {
                type: String,
            },

            num_bikes_available:{
                type: String
            },
            num_ebikes_available:{
                type: String
            },

            num_docks_available: {
                type: String
            },

            geometry: GeoSchema

});

module.exports = Bixi = mongoose.model('bixi_datas',BixiSchema);