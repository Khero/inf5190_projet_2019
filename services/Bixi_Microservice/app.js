
const express = require('express');
const connectDB = require ('./config/db');
const app = express();
const cors = require('cors');
swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger/BixiAPI.json');

//Connect Database
connectDB();


//init Middleware
app.use(cors());
app.use(express.json({ extended : false }));



app.get('/',(req,res) => res.send('API EN ROUTE'));
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Define Routes

app.use('/api/bixi', require('./Api/routes/bixi'));

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => console.log(`Server for Bixi_Microservice started on port ${PORT}`));