const Bixi = require('../../models/Bixi');
const request = require('request');
const bixidb = require('../../config/Bixi.json')
const bixidata = require('../../config/BixiData.json')

//This function format our orginal Json Bixi database nad insert it 
//in our mongodb
exports.filteredJson = async (req, res) => {
    try {
            
            const stations_bixi = bixidb.data.stations;
            const stations_Data = bixidata.data.stations;

            const json = stations_bixi.map((station,index) => {
                           
                            const stations  = { 
                                station_id: station.station_id,
                                name: station.name,
                                num_docks_available:stations_Data[index].num_docks_available,
                                num_ebikes_available:stations_Data[index].num_ebikes_available,
                                num_bikes_available:stations_Data[index].num_bikes_available,
                                "geometry.coordinates":[parseFloat(station.lon),parseFloat(station.lat)]
                                };
                                return new Bixi(stations);
                        
                    });
            await Bixi.insertMany(json);

            res.json(json);
        

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
 
//This function return all the station Bixi in our db.
exports.getAllStation = async(req,res) => { 

    try {
        
        let stations = await Bixi.find({});
        res.send(stations);

    } catch (err) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
}


//This function lookup for any point in the 250M radius
exports.distancePoint = async(req,res) => {
    try {
        let place_near = await Bixi.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [parseFloat(req.body.lon),parseFloat(req.body.lat)]
          },
          maxDistance: 250, // 250M
          spherical: true,
          distanceField: "distance"
        });
        res.json(place_near);
    } catch (error) {
        console.log(req.body.lon);
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
