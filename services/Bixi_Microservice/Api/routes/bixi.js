const express = require('express');
const router = express.Router();
const bixiController = require('../controllers/bixiController.js');



// @route   GET api/bixi/init
// @desc    sync and init mongo with bixiDb
// @access  Public
router.get('/init',bixiController.filteredJson);

// @route   GET api/bixi
// @desc    get all Stations in the database
// @access  Public
router.get('/',bixiController.getAllStation);



// @route   POST api/bixinearPoint
// @desc    get only the stations in 250m radius
// @access  Public
router.post('/nearPoint',bixiController.distancePoint);




module.exports = router;