const mongoose = require ('mongoose');



const GeoSchema = new mongoose.Schema({
    
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
      
})
const GardenSchema = new mongoose.Schema({
    
            OBJECTID: {
                type: String
            },

            Index: {
                type: String
            },
    
            Text_ : {
                type: String,
            },

            N_Jardinet:{
                type: String
            },
            Nbacs:{
                type: String
            },

            geometry: GeoSchema

});

module.exports = Garden = mongoose.model('garden_datas',GardenSchema);