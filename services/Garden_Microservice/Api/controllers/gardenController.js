const Garden = require('../../models/Garden');
const gardendb = require('../../config/Garden.json');
const polygonCenter = require('geojson-polygon-center');

exports.filteredJson = async (req, res) => {
    try {
      
            const gardens = gardendb.features;

            const json = gardens.map(garden => {
                const gardens  = { 
                            OBJECTID: garden.properties.OBJECTID,
                            Text_: garden.properties.Text_,
                            Index: garden.properties.Index,
                            N_Jardinet: garden.properties.N_Jardinet,
                            Nbacs: garden.properties.Nbacs,
                            //convert polygon geometry to point geometry
                            geometry: polygonCenter(garden.geometry)
                        };
                return new Garden(gardens);
            });

            await Garden.insertMany(json);
            res.json(json);
        

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
 

exports.getAllStation = async(req,res) => { 

    try {
        
        let gardens = await Garden.find({});
        res.send(gardens);

    } catch (err) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
}


exports.distancePoint = async(req,res) => {
    try {
        let place_near = await Garden.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [parseFloat(req.body.lon),parseFloat(req.body.lat)]
          },
          maxDistance: 250, // 300 KM
          spherical: true,
          distanceField: "distance"
        });
        res.json(place_near);
    } catch (error) {
        //console.log(req.body.lon);
        //console.log(req.body.lat);
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
