const express = require('express');
const router = express.Router();
const gardenController = require('../controllers/gardenController.js');



// @route   GET api/garden/ini
// @desc    sync and init mongo with gardenDb
// @access  Public
router.get('/init',gardenController.filteredJson);

// @route   GET api/garden
// @desc    get all Stations in the database
// @access  Public
router.get('/',gardenController.getAllStation);



// @route   POST api/garden/nearPoint
// @desc    get only the stations in 250m radius
// @access  Public
router.post('/nearPoint',gardenController.distancePoint);




module.exports = router;