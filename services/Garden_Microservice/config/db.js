const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');



mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.set('bufferCommands', false);

const connectDB = async () => {
    try{
       
        await mongoose.connect(db , {
            useNewUrlParser: true,
            dbName: 'garden_data'
        });


        console.log('MongoDB Connected For GARDEN_DATABASE...');
    }catch(err){
        console.error(err.message);
        process.exit(1);

    }
} 


module.exports = connectDB;