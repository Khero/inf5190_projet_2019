const Critics = require ('../../model/Crtitics');
const Rating = require ('../../model/Rating');


// Create of add comment in the database
exports.CreateAddComment = async (req, res) => {
    try {

        const newCritics = {

            
            place_id: req.body.place_id,
            place_name: req.body.place_name,
            comments: {
                user_id: req.body.user_id,
                user_name: req.body.user_name,
                text: req.body.text
            }
            
        }

        let critic = await Critics.findOne({place_id: req.body.place_id});
        //if we find a critic we update it , by adding the comment
        if(critic){

            critic.comments.unshift(newCritics.comments);
            await critic.save();
            return res.json(critic.comments);
        }
        critic = new Critics(newCritics);
        await critic.save();
        res.json(critic.comments);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
        
    }

}


exports.UpdateRating = async (req, res) => {

    try {
        let critic = await Rating.findOne({user_id: req.body.user_id , place_id: req.body.place_id});
      
        if(critic){
            critic = await Rating.findOneAndUpdate(
                {user_id: req.body.user_id , place_id: req.body.place_id},
                {$set: 
                    {rating: req.body.rating } 
                },
                {new: true})
                return res.json(critic);
        }
        const newRate = {
            user_id: req.body.user_id ,
            place_id: req.body.place_id,
            rating: req.body.rating
        }
        critic= new Rating(newRate);
        await critic.save();
        res.json(critic);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
    }
}


exports.DeleteComment = async (req, res) => {
   

    try {
        let critic = await Critics.findOne({place_id: req.body.place_id});
      
        //console.log(critic.id);
        //Get the remove index
    
            const removeIndex = await critic.comments.map(comment => comment.id).indexOf(req.body.comment_id);
        
            //If we find the index we remove it
            if(removeIndex != -1){
                critic.comments.splice(removeIndex,1);
                await critic.save();
            }
            
            //if there is no comment left we remove the User Critic
            if(critic.comments.length === 0){
                await Critics.findOneAndRemove({place_id: req.body.place_id});
            }
            
             res.json(critic.comments);
    

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
    }
}



exports.getAllCritics = async (req, res) => {

    try {
        let critic =  await Critics.find({});

        res.json(critic);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
    }


}


exports.getComment = async (req, res) => {
    try {

        let critic = await Critics.findById(req.params.id);
        
        res.json(critic);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
    }
}

exports.getAllCriticsForPlace = async (req, res) => {

    try {
        let critic1 =  await Critics.find({place_id: req.params.place_id});
        
        let comment = {
            user_comments:[]
        }

        let critics = critic1.map(critic => {
            return critic.comments.map(item =>{
                return item;
            })
        });

        return res.json(critics);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!')
    }


}


exports.getAllCriticsByUser = async (req, res) => {

    try {
        let critic =  await Critics.find({user_id: req.body.user_id});

        let comment = {
            user_comments:[]
        }

        let critics = critic.map(critic => {
            return critic.comments.map(item =>{
                return comment.user_comments.push(item);
            })
        });

        return res.json(comment.user_comments.sort().reverse());
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!');
    }

}

exports.getAvgRating = async (req, res) => {
        try {
            let critic =  await Rating.aggregate([
                 {
                     $match: {"place_id": req.body.place_id}
                    }, 

                 {
                     $group: 
                            {
                                _id: "$place_id",
                                avgRating: { $avg: "$rating" }
                            }
                }
            
            ])
               
            return res.json(critic);
            
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Oups server error !!!');
        }
}

exports.getRating = async (req, res) => {
    try {
        let critic =  await Rating.find({user_id: req.body.user_id,place_id: req.body.place_id});

        if(critic){
            return res.json(critic);
        }

        res.json({rating:0});
        
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!');
    }
}


