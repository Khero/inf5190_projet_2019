const express = require('express');
const router = express.Router();
const CriticsController = require('../controllers/CriticsController.js');




// @route   GET api/critics/all
// @desc    get all the critics from the database
// @access  public

router.get('/all', CriticsController.getAllCritics);


// @route   GET api/critics/:place_id
// @desc    Get all the place critics from the database
// @access  public

router.get('/place/:place_id', CriticsController.getAllCriticsForPlace);


// @route   GET api/critics/:user_id
// @desc    Get all the user critics from the database
// @access  public

router.get('/user', CriticsController.getAllCriticsByUser);


// @route   POST api/critics
// @desc    Create or add new comment
// @access  public

router.post('/addComment', CriticsController.CreateAddComment);


// @route   POST api/critics/rate
// @desc    Create or Update ratings
// @access  public

router.post('/rate', CriticsController.UpdateRating);


// @route   POST api/critics/delete/
// @desc    Delete comment by id
// @access  public

router.post('/delete', CriticsController.DeleteComment);

// @route   POST api/critics/avg
// @desc    get average rating a place places
// @access  public


router.post('/avg', CriticsController.getAvgRating);




// @route   post api/critics/comment
// @desc    post all Ratings by the user
// @access  public

router.post('/getRating', CriticsController.getRating);


module.exports = router;