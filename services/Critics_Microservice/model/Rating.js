const mongoose = require ('mongoose');

const RatingSchema = new mongoose.Schema({
    
    user_id: {
        type: String
    },
 
    place_id : {
        type: String,
    },

    rating:{
        type: Number
    }
   
});

module.exports = Rating = mongoose.model('rating_database',RatingSchema);