const mongoose = require ('mongoose');

const CriticsSchema = new mongoose.Schema({
    
  
    place_id : {
        type: String,
    },
    place_name : {
        type: String,
    },
    comments : [{

        user_id: {
            type: String
        },
        user_name : {
            type: String,
        },
        
        date: {
            type: Date,
            default: Date.now
        },

        text: {
            type: String,
            required: true
        }
    }]
    
});

module.exports = Critics = mongoose.model('critics_database',CriticsSchema);