const mongoose = require('mongoose');


const BailSchema = new mongoose.Schema({
 
   
    place_id : {
        type: String,
        required: true
    },
    place_name : {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
        },
    user_name : {
        type: String,
        required: true
        },
    date_start:{
        type: Date,
        default: Date.now
        },
    date_end:{
        type: Date,
        default: Date.now() + 365*24*60*60*1000
    }
    

});

module.exports = Bail = mongoose.model('Bail',BailSchema);