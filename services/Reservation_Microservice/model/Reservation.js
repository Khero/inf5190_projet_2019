const mongoose = require('mongoose');


const ReservationSchema = new mongoose.Schema({
 
   
    place_id : {
        type: String
    },
    place_name : {
        type: String
    },

    user_id: {
        type: String
    },
    user_name : {
        type: String
    },
    date:{
        type: Date,
        default:Date.now
    }

    
    

});

module.exports = Reservation = mongoose.model('reseservation',ReservationSchema);