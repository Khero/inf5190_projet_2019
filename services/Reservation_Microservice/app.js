
const express = require('express');
const connectDB = require ('./config/db');
const app = express();
var cors = require('cors');
swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger/ReservationAPI.json');

//Connect Database
connectDB();


//init Middleware
app.use(cors());
app.use(express.json({ extended : false }));



app.get('/',(req,res) => res.send('API EN ROUTE'));
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// Define Routes

app.use('/api/reservation', require('./Api/routes/reservation'));

const PORT = process.env.PORT || 4250;

app.listen(PORT, () => console.log(`Server for Reservation_Microservice started on port ${PORT}`));