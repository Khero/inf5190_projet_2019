
const Reservation = require('../../model/Reservation');
const Bail = require('../../model/Bail');


exports.CreateReservation = async(req,res ) => {

    const newReservation = {            
        place_id: req.body.place_id,
        place_name: req.body.place_name,
        user_id: req.body.user_id,
        user_name: req.body.user_name,
        date_start: req.body.start

        }
        
        try {
            let reservation = await Reservation.findOne({ user_id: req.body.user_id, place_id:req.body.place_id});
            
            if(reservation){
                return res.status(400).json({ errors : [{ msg: 'User has already a reservation'}] });
            }

            reservation = new Reservation(newReservation);
            await reservation.save();
            let reservations = await Reservation.find({ user_id: req.body.user_id});
            res.json(reservations);

        } catch (err) {
            console.error(err.message);
            res.status(500).send('Oups server error !!!');
            
        }



}

exports.SignBail = async( req, res ) => {


    const newBail = {            
        place_id: req.body.place_id,
        place_name: req.body.place_name,
        user_id: req.body.user_id,
        user_name: req.body.user_name,

        }
        
        try {
            let bail = await Bail.findOne({ user_id: req.body.user_id, place_id:req.body.place_id});
            
            if(bail){
                return res.status(400).json({ errors : [{ msg: 'User has already a bail'}] });
            }

            bail = new Bail(newBail);
            await bail.save();
            let bails = await Bail.find({ user_id: req.body.user_id});
            res.json(bails);

        } catch (err) {
            console.error(err.message);
            res.status(500).send('Oups server error !!!');
            
        }
        
}






exports.getMySignedBail= async( req, res) => {
  
        try {
            let bail = await Bail.find({ user_id: req.body.user_id});
          
            res.json(bail);

        } catch (err) {
            console.error(err.message);
            res.status(500).send('Oups server error !!!');
            
        }

}

exports.getMyReservation = async( req, res) => {
  
    try {
        let reservation = await Reservation.find({ user_id: req.body.user_id});
      
        res.json(reservation);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!');
        
    }

}

exports.getNumReservation= async( req,res) => {
    //console.log(req.body.place_id);
    try {
        let reservation = await Reservation.aggregate(
            [
                
                {
                    $match: {"place_id": req.body.place_id}
                }, 
              {
                $count: "reservations"
              }
            ]
          )
          
        res.json(reservation);

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!');
        
    }

}

exports.DeleteReservation= async( req,res) => {
    try {
        let reservation = await Reservation.findOneAndDelete({ user_id: req.body.user_id, place_id:req.body.place_id});
        let reservations = await Reservation.find({ user_id: req.body.user_id});
        if(!reservation){
            return res.status(400).json({ errors : [{ msg: 'You have no reservation at this place'}] });
        }
        res.json(reservations)

    } catch (err) {
        console.error(err.message);
        res.status(500).send('Oups server error !!!');
        
    }

}
