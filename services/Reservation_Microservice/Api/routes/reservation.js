const express = require('express');
const router = express.Router();
const ReservationController = require('../controllers/ReservationController');


// @route   POST api/reservation/newBail
// @desc    Create a new bail
// @access  Public
router.post('/newBail', ReservationController.SignBail);

// @route   POST api/reservation/create
// @desc    Create a new reservation
// @access  Public
router.post('/create', ReservationController.CreateReservation);


// @route   POST api/reservation/sum
// @desc    get number of reservation pending for a place
// @access  Public
router.post('/sum', ReservationController.getNumReservation);

// @route   POST api/reservation/bail
// @desc    Get all the bail signer for a user
// @access  Public
router.post('/bail', ReservationController.getMySignedBail);

// @route   POST api/reservation/reserve
// @desc    Get all the reservation for a user
// @access  Public
router.post('/reserve', ReservationController.getMyReservation);

// @route   POST api/reservation/del
// @desc    Delete a reservation
// @access  Public
router.post('/del', ReservationController.DeleteReservation);


module.exports = router;