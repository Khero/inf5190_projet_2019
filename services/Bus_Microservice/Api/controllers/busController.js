const Bus = require('../../models/Bus');
const busdb = require('../../config/Bus.json')

//This function format our orginal Json bus database nad insert it 
//in our mongodb
exports.filteredJson = async (req, res) => {
    try {
     
            const stations_bus = busdb.features;

            const json = stations_bus.map(station => {
                const stations  = { 
                        stop_id: station.properties.stop_id,
                        stop_code: station.properties.stop_code,
                        stop_name:  station.properties.stop_name,
                        geometry: station.geometry
                        };
                return new Bus(stations);
            });
            await Bus.insertMany(json);
            res.json(json);
        

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
 
//This function return all the bus stations in our db.
exports.getAllStation = async(req,res) => { 

    try {
        
        let stations = await Bus.find({});
        res.send(stations);

    } catch (err) {
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
}

//This function lookup for any point in the 250M radius
exports.distancePoint = async(req,res) => {
    try {
        let place_near = await Bus.aggregate()
        .near({
          near: {
            type: "Point",
            coordinates: [parseFloat(req.body.lon),parseFloat(req.body.lat)]
          },
          maxDistance: 250, // 250M
          spherical: true,
          distanceField: "distance"
        });
        res.json(place_near);
    } catch (error) {
        console.log(req.body);
        console.error(error.message);
        res.status(500).send('Oups server error !');
    }
    
}
