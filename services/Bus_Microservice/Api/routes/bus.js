const express = require('express');
const router = express.Router();
const busController = require('../controllers/busController.js');



// @route   GET api/bus
// @desc    sync and init mongo with busDb
// @access  Public
router.get('/init',busController.filteredJson);

// @route   GET api/bus
// @desc    get all Stations in the database
// @access  Public
router.get('/',busController.getAllStation);



// @route   GET api/bus
// @desc    get only the stations in 250m radius
// @access  Public
router.post('/nearPoint',busController.distancePoint);




module.exports = router;