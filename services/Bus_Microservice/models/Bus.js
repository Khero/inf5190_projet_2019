const mongoose = require ('mongoose');



const GeoSchema = new mongoose.Schema({
    
    type: {
        type: String,
        default: "Point"
    },
    coordinates: {
        type: [Number],
        index: "2dsphere"
    }
      
})
const BusSchema = new mongoose.Schema({
    
            stop_id: {
                type: String
            },
            stop_code : {
                type: String,
            },
            stop_name : {
                type: String,
            },
          
            geometry: GeoSchema

});

module.exports = Bus = mongoose.model('bus_database',BusSchema);